function res = fCint(n,m,pars,z,x)

  res = ksi_n_ksi_j_conj(n,m,pars,x).*epsilon(x,z,pars);

end