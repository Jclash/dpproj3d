clear all
%pars = struct('lam',2*pi,'k',1,'alpha',0,'a',2*pi);
pars = struct();

pars.('lam') = 2*pi
pars.('k') = 2*pi/pars.('lam')
pars.('alpha') = 30.0*pi/180
pars.('theta') = 0.0*pi/180
pars.('a1') = 2*pi;
pars.('a2') = 2*pi;
pars.('x3_min') = -1.0
pars.('x3_max') = 2.0
pars.('epsilon') = 3.0% + 115.0*1i

pars.('E01') = 0.0
pars.('E02') = 1.0
pars.('E03') = 0.0

a1 = pars.('a1') 
a2 = pars.('a1') 
k = pars.('k')
eps = pars.('epsilon')
alpha = pars.('alpha')
x3_min = pars.('x3_min')
x3_max = pars.('x3_max')
E01 = pars.('E01')
E02 = pars.('E02')
E03 = pars.('E03')

%N = 1
N1 = -floor(k*(1+sin(alpha)))+1
N2 = floor(k*(1-sin(alpha)))-1
NN = N2 - N1 + 1;

lamt = linspace(1,NN,NN);
lambda_n(lamt)= (k*a*sin(alpha)+2*pi*(N1+lamt-1))/a; 
gamma_n = sqrt(k^2 - lambda_n.*lambda_n);


sol=bvpinit(linspace(0,1,25),[0 1]);
sol=bvp4c(@bvp3d,@bc,sol);
sol.x