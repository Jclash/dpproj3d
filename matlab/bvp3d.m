function yprime = bvp3d(t,y)
yprime = [y(2);-2*y(1)+3*y(2)];