
function res = ksi_n_ksi_j_conj(n,j,pars,x)
  res1 = ksi_n(n,x,pars);
  res2 = ksi_n(j,x,pars);
  res = res1.*conj(res2);
  %res = ksi_n(n,x,pars)*conj(ksi_n(j,x,pars));
end