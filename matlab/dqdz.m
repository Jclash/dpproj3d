function dqdz=dqdz(z,q,C,cspan)
%   C11 = interp1(cspan,reshape(C(1,1,:),1,20),z);
%   C12 = interp1(cspan,reshape(C(1,2,:),1,20),z);
%   C13 = interp1(cspan,reshape(C(1,3,:),1,20),z);
%   C21 = interp1(cspan,reshape(C(2,1,:),1,20),z);
%   C22 = interp1(cspan,reshape(C(2,2,:),1,20),z);
%   C23 = interp1(cspan,reshape(C(2,3,:),1,20),z);
%   C31 = interp1(cspan,reshape(C(3,1,:),1,20),z);
%   C32 = interp1(cspan,reshape(C(3,2,:),1,20),z);
%   C33 = interp1(cspan,reshape(C(3,3,:),1,20),z);
  
  Nv = numel(q)/2
  v = zeros(1,Nv*2);
  
  Nz = numel(cspan);
  for j = 1:Nv
    for n = 1:Nv
        Cnjz = interp1(cspan,reshape(C(n,j,:),1,Nz),z);
        v(j) = v(j) - Cnjz*q(Nv+n);
        v(j+Nv) = q(j);
    end
  end
  
  dqdz = v.';
  
%   dqdz = [ -C11*q(4)-C21*q(5)-C31*q(6)
%            -C12*q(4)-C22*q(5)-C32*q(6)
%            -C13*q(4)-C23*q(5)-C33*q(6)
%            q(1)
%            q(2)
%            q(3) ];
end
