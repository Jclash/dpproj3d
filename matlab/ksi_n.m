
function res = ksi_n(n,x,p)
  t = p.('k')*p.('a')*sin(p.('alpha'));
  res = 1/sqrt(p.('a'))*exp(1i*(t+2*pi*n)/p.('a').*x);
end