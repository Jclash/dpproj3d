
x = 0:0.005:a+0.005;
hold on
for n = -N:1:N
  y = ksi_n(n,x,pars); 
  plot(x,y)
end
grid on
legend('-1','0','1','Location','NorthWestOutside')
hold off