clear all
%pars = struct('lam',2*pi,'k',1,'alpha',0,'a',2*pi);
pars = struct();

pars.('lam') = 2*pi
pars.('k') = 2*pi/pars.('lam')
pars.('alpha') = 0*pi/180
pars.('a') = 2*pi; %2*pi/(pars.('k')*sin(pars.('alpha')))
pars.('z_min') = -1.0
pars.('z_max') = 2.0
pars.('epsilon') = 3.0% + 115.0*1i
pars.('C0') = exp(1i)

a = pars.('a') 
k = pars.('k')
eps = pars.('epsilon')
alpha = pars.('alpha')
z_min = pars.('z_min')
z_max = pars.('z_max')
C0 = pars.('C0')

%N = 1
N1 = -floor(k*(1+sin(alpha)))+1
N2 = floor(k*(1-sin(alpha)))-1
NN = N2 - N1 + 1;

lamt = linspace(1,NN,NN);
lambda_n(lamt)= (k*a*sin(alpha)+2*pi*(N1+lamt-1))/a; 
gamma_n = sqrt(k^2 - lambda_n.*lambda_n);

%%fun = @(x) ksi_n(2,x,pars);
%integral(@(x)ksi_n_ksi_j_conj(2,-3,pars,x),0,a) 

%
% CALCULATING C MATRIX
%
Nz = 4800;
zspan = linspace(z_min,z_max,Nz);
dz = (z_max - z_min)/(Nz - 1);
for n = 1:NN
    for j = 1:NN
        for m = 1:Nz            
            z = z_min + dz*(m-1);
            %fint = @(x) ksi_n_ksi_j_conj(n-N-1,m-N-1,pars,x)*epsilon(x,z,pars)
            C(n,j,m) = k^2*integral( @(x) fCint(N1+n-1,N1+j-1,pars,z,x),0,a) - lambda_n(j)^2*kroneckerDelta(n,j);
        end
    end
end
% zs = linspace(z_min,z_max,250);
% ffz = @(z) epsilon(0,z,pars)
% for m = 1:250
%   fz(m) = ffz(zs(m))
% end
% %fz = sin(zs)
% plot(zs,fz);

% for m = 1:Nz
%   z = z_min + dz*(m-1);
%   fz(m) = interp1(zspan,reshape(C(1,3,:),1,20),z);
% end
% plot(zspan,fz);

%plot(zspan, reshape(C(1,1,:),1,20))


tspan = [z_min z_max];
%options = odeset('Refine',10);

Nx = 4800;
x = linspace(z_min,z_max,Nx);

Qsol = zeros(2*NN, 2*NN, Nx);

q0 = [0; 1];
options = odeset('RelTol',1e-10)
sol1 = ode15s( @(z,q) dqdz(z,q,C,zspan), tspan, q0, options);
Q1 = deval(sol1,x);
Qsol(1,:,:) = Q1;
%plot(x,Q1(2,:));

q0 = [1; 0];
sol2 = ode45( @(z,q) dqdz(z,q,C,zspan), tspan, q0);
Q2 = deval(sol2,x);
Qsol(2,:,:) = Q2;

% q0 = [0; 0; 1; 0; 0; 0];
% sol3 = ode45( @(z,q) dqdz(z,q,C,zspan), tspan, q0);
% Q3 = deval(sol3,x);
% Qsol(3,:,:) = Q3;
% 
% q0 = [0; 0; 0; 1; 0; 0];
% sol4 = ode45( @(z,q) dqdz(z,q,C,zspan), tspan, q0);
% Q4 = deval(sol4,x);
% Qsol(4,:,:) = Q4;
% 
% q0 = [0; 0; 0; 0; 1; 0];
% sol5 = ode45( @(z,q) dqdz(z,q,C,zspan), tspan, q0);
% Q5 = deval(sol5,x);
% Qsol(5,:,:) = Q5;
% 
% q0 = [0; 0; 0; 0; 0; 1];
% sol6 = ode45( @(z,q) dqdz(z,q,C,zspan), tspan, q0);
% Q6 = deval(sol6,x);
% Qsol(6,:,:) = Q6;

% Qt1 = -1i*gamma_n(1)*Q1(:,250) + Q4(:,250)
% Qt2 = -1i*gamma_n(2)*Q2(:,250) + Q5(:,250)
% Qt3 = -1i*gamma_n(3)*Q3(:,250) + Q6(:,250)
% 
% A11 = Qt1(1) - 1i*gamma_n(1)*Qt1(4)
% A12 = Qt2(1) - 1i*gamma_n(1)*Qt2(4)
% A13 = Qt3(1) - 1i*gamma_n(1)*Qt3(4)
% A21 = Qt1(2) - 1i*gamma_n(2)*Qt1(5)
% A22 = Qt2(2) - 1i*gamma_n(2)*Qt2(5)
% A23 = Qt3(2) - 1i*gamma_n(2)*Qt3(5)
% A31 = Qt1(3) - 1i*gamma_n(3)*Qt1(6)
% A32 = Qt2(3) - 1i*gamma_n(3)*Qt2(6)
% A33 = Qt3(3) - 1i*gamma_n(3)*Qt3(6)
% 
% A = [ A11, A12, A13; A21, A22, A23; A31, A32, A33 ]
% 
% b(1) = -2*1i*gamma_n(1)*exp(-1i*gamma_n(1)*z_max)*U0
% b(2) = -2*1i*gamma_n(2)*exp(-1i*gamma_n(2)*z_max)*U0
% b(3) = -2*1i*gamma_n(3)*exp(-1i*gamma_n(3)*z_max)*U0

A = zeros(2*NN,2*NN);
b = zeros(1,2*NN);
for j = 1:NN
    for s = 1:2*NN
        A(j,s) = Qsol(s,j,1) + 1i*gamma_n(j)*Qsol(s,j+NN,1);
        A(j+NN,s) = Qsol(s,j,Nx) - 1i*gamma_n(j)*Qsol(s,j+NN,Nx);
        b(j) = 0;
        b(j+NN) = 0;
        if j == -N1+1
            b(j+NN) = -2*1i*C0*sqrt(a)*gamma_n(-N1+1)*exp(-1i*gamma_n(-N1+1)*z_max);
        end
        %b(j+NN) = -2*1i*gamma_n(j)*exp(-1i*gamma_n(j)*z_max)*U0;
    end
end
alphas = A\b.'

% Qtf1 = -1i*gamma_n(1)*Q1 + Q4
% Qtf2 = -1i*gamma_n(2)*Q2 + Q5
% Qtf3 = -1i*gamma_n(3)*Q3 + Q6
%%Q = alphas(1)*Q1 + alphas(2)*Q2 + alphas(3)*Q3 + alphas(4)*Q4 + alphas(5)*Q5 + alphas(6)*Q6

Q = alphas(1)*Qsol(1,:,:);
for s = 2:2*NN
  Q = Q + alphas(s)*Qsol(s,:,:);
end

for s = 1:Nx
  Qp(s) = Q(1,2,s);
end

plot(x,abs(Qp))

d1 = 2.15654 + 0.3038i
d2 = 0.3038 - 2.15654i
d3 = 1.24508 + 0.175399i
d4 = 0.3038 - 2.15654i
d5 = 1.16869 - 2.16401i
d6 = 0.544668 + 3.04982i

Qmm(1:Nx/3) = d1*sin(x(1:Nx/3)) - d2*cos(x(1:Nx/3))
Qmm(Nx/3+1:2*Nx/3) = d3*sin(sqrt(eps)*x(Nx/3+1:2*Nx/3)) - d4*cos(sqrt(eps)*x(Nx/3+1:2*Nx/3))
Qmm(2*Nx/3+1:Nx) = d5*sin(x(2*Nx/3+1:Nx)) + d6*cos(x(2*Nx/3+1:Nx))

%plot(x(1:Nx/3),abs(Qp(1:Nx/3)-Qmm(1:Nx/3)))
plot(x,abs(Qp-Qmm))


% %integral(@(z) interp1(x,reshape(Q(1,5,:),1,250),z).*conj(interp1(x,reshape(Q(1,5,:),1,250),z)),z_min,z_max) 
% R1 = Q(1,4,Nx)*conj(Q(1,4,Nx))
% R2 = (Q(1,5,Nx)/sqrt(a)*exp(-1i*gamma_n(2)*z_max)-exp(-1i*2*gamma_n(2)*z_max))%*conj(Q(1,5,250)/sqrt(a)*exp(-1i*gamma_n(2)*z_max)-exp(-1i*2*gamma_n(2)*z_max))
% R3 = Q(1,6,Nx)*conj(Q(1,6,Nx))
% 
% T1 = Q(1,4,1)*conj(Q(1,4,1))/a
% T2 = Q(1,5,1)*conj(Q(1,5,1))/a
% T3 = Q(1,6,1)*conj(Q(1,6,1))/a
% 
% Rn = Q(1,5,Nx)*exp(-1i*gamma_n(2)*z_max)/sqrt(a)
% 
% def = R2*conj(R2)
% for s=1:NN
%     if s ~= -N1+1
%       def = def + Q(1,NN+s,Nx)*conj(Q(1,NN+s,Nx))/a;
%     end
% end
% def = 1 - def
% 
% %1 - (Q(1,4,250)*conj(Q(1,4,250))/a + Q(1,5,250)*conj(Q(1,5,250))/a + Q(1,6,250)*conj(Q(1,6,250))/a) ...
% %  - (Q(1,4,1)*conj(Q(1,4,1))/a + Q(1,5,1)*conj(Q(1,5,1))/a + Q(1,6,1)*conj(Q(1,6,1))/a)
% 
% interp1(x,reshape(Q(1,:),1,Nx),z)*conj(interp1(x,reshape(Q(1,:),1,Nx),z));
% 
% integral(@(z) interp1(x,reshape(Q(4,:),1,250),z).*conj(interp1(x,reshape(Q(4,:),1,250),z)),z_min,z_max) 
% integral(@(z) interp1(x,reshape(Q(5,:),1,250),z).*conj(interp1(x,reshape(Q(5,:),1,250),z)),z_min,z_max) 
% integral(@(z) interp1(x,reshape(Q(6,:),1,250),z).*conj(interp1(x,reshape(Q(6,:),1,250),z)),z_min,z_max) 
% 
% %A*alphas-b.'
% 
% interp1(x,reshape(Q(1,:),1,250),z_max-0.25)*conj(interp1(x,reshape(Q(1,:),1,250),z_max-0.25))
% 
% %
% y = deval(sol5,x);
% plot(x,y(4,:));
% 
% %plot_epsilon
% 
% %plot_ksi_n;
% 
% 
% 
