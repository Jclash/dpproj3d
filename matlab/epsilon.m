function res = epsilon(x,z,p)
  
  if z > 1.0
    res = 1.0;
  else
  if z < 0.0
    res = 1.0;
  else
    res = p.('epsilon');
  end
  end
  
end