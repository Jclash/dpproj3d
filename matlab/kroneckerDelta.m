
function res = kroneckerDelta(n1,n2)
  if n1 == n2 
    res = 1;
  else
    res = 0;
  end
end