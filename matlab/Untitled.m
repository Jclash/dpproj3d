clear all
%pars = struct('lam',2*pi,'k',1,'alpha',0,'a',2*pi);
pars = struct();

pars.('lam') = 2*pi
pars.('k') = 2*pi/pars.('lam')
pars.('alpha') = (180-0)*pi/180
pars.('a') = 2*pi; %2*pi/(pars.('k')*sin(pars.('alpha')))
pars.('z_min') = -4.0
pars.('z_max') = 2.0
pars.('epsilon') = 49.0 %+ 115.0*1i
pars.('U0') = 1.0

a = pars.('a') 
k = pars.('k')
alpha = pars.('alpha')
z_min = pars.('z_min')
z_max = pars.('z_max')
U0 = pars.('U0')

N = 1
N1 = -floor(a*k*(1+sin(alpha))/2/pi)
N2 = floor(a*k*(1-sin(alpha))/2/pi)

lamt = linspace(1,2*N+1,2*N+1);
lambda_n(lamt)= (k*a*sin(alpha)+2*pi*(lamt-N-1))/a; 
gamma_n = sqrt(k^2 - lambda_n);

%%fun = @(x) ksi_n(2,x,pars);
%integral(@(x)ksi_n_ksi_j_conj(2,-3,pars,x),0,a) 

%
% CALCULATING C MATRIX
%
Nz = 20;
zspan = linspace(z_min,z_max,Nz);
dz = (z_max - z_min)/(Nz - 1);
for n = 1:2*N+1
    for j = 1:2*N+1
        for m = 1:Nz            
            z = z_min + dz*(m-1);
            C(n,j,m) = k^2*integral(@(x) ksi_n_ksi_j_conj(n-N-1,m-N-1,pars,x)*epsilon(x,z,pars) ,0,a) - lambda_n(j)^2*kroneckerDelta(n,j);
        end
    end
end
%plot(reshape(C11(1,1,:),1,20))

tspan = [z_min z_max];
%options = odeset('Refine',10);

x = linspace(z_min,z_max,250);

q0 = [1; 0; 0; 0; 0; 0];
sol1 = ode45( @(z,q) dqdz(z,q,C,zspan), tspan, q0);
Q1 = deval(sol1,x);

q0 = [0; 1; 0; 0; 0; 0];
sol2 = ode45( @(z,q) dqdz(z,q,C,zspan), tspan, q0);
Q2 = deval(sol2,x);

q0 = [0; 0; 1; 0; 0; 0];
sol3 = ode45( @(z,q) dqdz(z,q,C,zspan), tspan, q0);
Q3 = deval(sol3,x);

q0 = [0; 0; 0; 1; 0; 0];
sol4 = ode45( @(z,q) dqdz(z,q,C,zspan), tspan, q0);
Q4 = deval(sol4,x);

q0 = [0; 0; 0; 0; 1; 0];
sol5 = ode45( @(z,q) dqdz(z,q,C,zspan), tspan, q0);
Q5 = deval(sol5,x);

q0 = [0; 0; 0; 0; 0; 1];
sol6 = ode45( @(z,q) dqdz(z,q,C,zspan), tspan, q0);
Q6 = deval(sol6,x);

Qt1 = -1i*gamma_n(1)*Q1(:,250) + Q4(:,250)
Qt2 = -1i*gamma_n(2)*Q2(:,250) + Q5(:,250)
Qt3 = -1i*gamma_n(3)*Q3(:,250) + Q6(:,250)

A11 = Qt1(1) - 1i*gamma_n(1)*Qt1(4)
A12 = Qt2(1) - 1i*gamma_n(1)*Qt2(4)
A13 = Qt3(1) - 1i*gamma_n(1)*Qt3(4)
A21 = Qt1(2) - 1i*gamma_n(2)*Qt1(5)
A22 = Qt2(2) - 1i*gamma_n(2)*Qt2(5)
A23 = Qt3(2) - 1i*gamma_n(2)*Qt3(5)
A31 = Qt1(3) - 1i*gamma_n(3)*Qt1(6)
A32 = Qt2(3) - 1i*gamma_n(3)*Qt2(6)
A33 = Qt3(3) - 1i*gamma_n(3)*Qt3(6)

A = [ A11, A12, A13; A21, A22, A23; A31, A32, A33 ]

b(1) = -2*1i*gamma_n(1)*exp(-1i*gamma_n(1)*z_max)*U0
b(2) = -2*1i*gamma_n(2)*exp(-1i*gamma_n(2)*z_max)*U0
b(3) = -2*1i*gamma_n(3)*exp(-1i*gamma_n(3)*z_max)*U0

b.'

alphas = A\b.'

Qtf1 = -1i*gamma_n(1)*Q1 + Q4
Qtf2 = -1i*gamma_n(2)*Q2 + Q5
Qtf3 = -1i*gamma_n(3)*Q3 + Q6
Q = alphas(1)*Qtf1 + alphas(2)*Qtf2 + alphas(3)*Qtf3

interp1(x,reshape(Q(1,:),1,250),z)*conj(interp1(x,reshape(Q(1,:),1,250),z));

integral(@(z) interp1(x,reshape(Q(4,:),1,250),z).*conj(interp1(x,reshape(Q(4,:),1,250),z)),z_min,z_max) 
integral(@(z) interp1(x,reshape(Q(5,:),1,250),z).*conj(interp1(x,reshape(Q(5,:),1,250),z)),z_min,z_max) 
integral(@(z) interp1(x,reshape(Q(6,:),1,250),z).*conj(interp1(x,reshape(Q(6,:),1,250),z)),z_min,z_max) 

%A*alphas-b.'



%
%y = deval(sol1,x);
%plot(x,y(5,:));

%plot_epsilon

%plot_ksi_n;



