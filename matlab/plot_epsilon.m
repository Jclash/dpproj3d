
pzmin = -10
pzmax = 10
pxmin = -10
pxmax = 10

x = pxmin:0.1:pxmax;
z = pzmin:0.1:pzmax;
nx = 1;
hold on
for ii = 1:numel(x)
  for jj = 1:numel(z)
    xval = x(ii);
    zval = z(jj);
    val = epsilon(xval,zval,pars); 
    if val == 1
        %scatter(zval,xval,'g','.');
    else
        xs(nx) = xval;
        zs(nx) = zval;
        nx = nx+1;
    end
  end
end
scatter(zs,xs,'.','r');
plot([z_min,z_min],[pxmin,pxmax],'--k');
plot([z_max,z_max],[pxmin,pxmax],'--k');
plot([0,0],[pxmin,pxmax],'k');
plot([pzmin,pzmax],[0,0],'k');
plot([pzmin,pzmax],[a,a],'--k');
plot([pzmin,pzmax],[a,a],'--k');
quiver( pzmax,0,k*cos(alpha),k*sin(alpha),0 )
axis([-10 10 -10 10])
grid on
hold off