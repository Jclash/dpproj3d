CC = g++
#CC = x86_64-w64-mingw32-c++

MPIINCLUDE=-I/usr/include/mpich

#GSLINCLUDE=-I/usr/include/
GSLINCLUDE1=-I/nfs/hybrilit.jinr.ru/user/k/knyazkov/soft/include
GSLINCLUDE2=-I/usr/include
GSLINCLUDE3=-I/home4/pstorage1/ipmex4/include

#CFLAGS = $(MPIINCLUDE) -fopenmp 
CFLAGS = $(GSLINCLUDE1) $(GSLINCLUDE2) $(GSLINCLUDE3)

# Hydra
GSLLIBS1 = -L/nfs/hybrilit.jinr.ru/user/k/knyazkov/soft/lib
# Lenovo Cygwin
GSLLIBS2 = -L/lib
# JSCC
GSLLIBS3 = -L/home4/pstorage1/ipmex4/lib

LFLAGS = $(GSLLIBS1) $(GSLLIBS2) $(GSLLIBS3)

EXETARGET = tuvp2vr.exe

# Lenovo Laptop Cygwin
#PROJ_PATH = /home/User/work/dpproj

# Hydra
#PROJ_PATH = ~/work/dpproj

# Ubuntu
#PROJ_PATH = ~/work/dpproj

# LENOVO-PC
#PROJ_PATH = ~/work/dpproj_new/dpproj

# MSC
PROJ_PATH = .

SRC_PATH = $(PROJ_PATH)/ConsoleApplication2/ConsoleApplication2
BIN_PATH = $(PROJ_PATH)/bin

#
# Video encoder:
#   avconv - for linux
#   ffmpeg.exe - for windows
#
AVICONV = ffmpeg.exe 
AVIRATE = 3
AVIOPT = -framerate 25 -r $(AVIRATE) -i

.PHONY: plots, clean, clobber

run: build
	./rs
#	$(BIN_PATH)/run


build: $(BIN_PATH)/run

$(BIN_PATH)/ConsoleApplication2.o : $(SRC_PATH)/ConsoleApplication2.cpp $(SRC_PATH)/stdafx.h
	$(CC) $(CFLAGS) -c $(SRC_PATH)/ConsoleApplication2.cpp -o $(BIN_PATH)/ConsoleApplication2.o
	
$(BIN_PATH)/dpproj.o : $(SRC_PATH)/dpproj.cpp $(SRC_PATH)/dpproj.h
	$(CC) $(CFLAGS) -c $(SRC_PATH)/dpproj.cpp -o $(BIN_PATH)/dpproj.o	

$(BIN_PATH)/run: $(BIN_PATH)/ConsoleApplication2.o $(BIN_PATH)/dpproj.o
	$(CC) $(LFLAGS) $(BIN_PATH)/ConsoleApplication2.o $(BIN_PATH)/dpproj.o -o $(BIN_PATH)/run -lgsl -lgslcblas -lm

plots: run plotscript
	@echo 
	@echo Plotting results...
	gnuplot plotscript
#>  plot  "C0.txt" using 1:1 title 'x', "C0.txt" using 1:2 title 'Cnj'	


plotlayer:  $(PROJ_PATH)/exp0_figlayer.png
	
	
$(PROJ_PATH)/exp0_figlayer.png: $(PROJ_PATH)/exp0_epsilon.txt
	gnuplot -e "expname='exp0'" plotlayer

$(PROJ_PATH)/exp1_figlayer.png: $(PROJ_PATH)/exp1_epsilon.txt
	gnuplot -e "expname='exp1'" plotlayer	
	
$(PROJ_PATH)/exp0_epsilon.txt: $(BIN_PATH)/run $(PROJ_PATH)/input.txt
	./rs
	
$(PROJ_PATH)/exp1_epsilon.txt: $(BIN_PATH)/run $(PROJ_PATH)/input.txt
	./rs
	
#$(PROJ_PATH)/exp*_figlayer.png: $(PROJ_PATH)/exp*_epsilon.txt
#	gnuplot $(inputs) plotlayer

plotlayers:  $(PROJ_PATH)/exp0_figlayer.png $(PROJ_PATH)/exp1_figlayer.png 
	
	
exp*_figlayer.png: $(PROJ_PATH)/exp*_epsilon.txt
	gnuplot $(input) -o $(output)

	
webplot: B0_abs.txt B0_arg.txt plotscriptweb
	@echo 
	@echo Plotting web picture...
	gnuplot plotscriptweb

kosmonitfig1: KOSMONIT_figDefFromAlpha_def.txt run plotdeffromalpha
	@echo 
	@echo Plotting web picture...
	gnuplot plotdeffromalpha
	
	
tuvp2vr.exe: ./Release/tuvp2vr.exe 
	rm -f tuvp2vr.exe
	cp ./Release/tuvp2vr.exe ./

solution.txt: $(EXETARGET)
	@echo 
	@echo Running code...
	rm -f step*.txt
	tuvp2vr.exe
	cat ha > solution.txt

plots.png: solution.txt plot_solution.py
	@echo 
	@echo Plotting results...
	rm -f plot*.png
	rm -f centerline*.png
	python plot_solution.py
	cat ha > plots.png

#
# copy from animate target:
#
#python animate_solution.py	
#avconv -i plot_r%d.png solution_r.avi
#avconv -i plot_r_mpi%d.png solution_r_mpi.avi
#avconv -i plocm_I1%d.png solution_I1.avi
#avconv -i plocm_sigma_e%d.png solution_sigma_e.avi
#avconv -f image2 -i plotcm_I1%d.png -vcodec h264 -crf 1 -r 24 out.mov
#avconv -i plotcm_I1%d.png -s solution_I1.avi
#avconv -i plotcm_sigma_e%d.png solution_sigma_e.avi

	
animate: solution_T0.avi solution_I1.avi solution_taup_rr_t.avi solution_sigma_e.avi solution_S.avi solution_T0_t.avi
	@echo 
	@echo Animating results...
#	rm -f *.avi

solution_T0.avi: plots.png	
	rm -f solution_T0.avi	
	$(AVICONV) $(AVIOPT) plotcm_T0%d.png solution_T0.avi
	
solution_I1.avi: plots.png	
	rm -f solution_I1.avi
	$(AVICONV) $(AVIOPT) plotcm_I1%d.png solution_I1.avi

solution_taup_rr_t.avi:	plots.png
	rm -f solution_taup_rr_t.avi
	$(AVICONV) $(AVIOPT) plotcm_taup_rr_t%d.png solution_taup_rr_t.avi

solution_sigma_e.avi: plots.png
	rm -f solution_sigma_e.avi
	$(AVICONV) $(AVIOPT) plotcm_Sigma_e%d.png solution_sigma_e.avi

solution_T0_t.avi: plots.png	
	rm -f solution_T0_t.avi
	$(AVICONV) $(AVIOPT) plotcm_T0_t%d.png solution_T0_t.avi	

solution_S.avi:	plots.png
	rm -f solution_S.avi	
	$(AVICONV) $(AVIOPT) plotcm_S%d.png solution_S.avi	

clean:
	rm -f $(BIN_PATH)/*.o $(BIN_PATH)/run

clobber: clean
	rm -f solution.txt *.png

resclean:
	rm -f *.png Q*.txt B*.txt C*.txt fig*.txt *.out *.avi solution.txt plots Tmax*.txt
	rm -f run.*/*
	rm -f run.*/.hosts
	rmdir run.*

imgclean:
	rm -f *.png 	
	
allclean: clean resclean
	rm *~ *dump ./set
