
#include "stdafx.h"
#include "dpproj.h"
#include "string.h"

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <string>
using namespace std;

#define _USE_MATH_DEFINES
#include <math.h>

#include <sstream>
#include <ctime>

//#include "mpi.h"

int textout;
bool layerout = false;

double diffclock(double clock1, double clock2)
{
	//return ((clock2-clock1)*1000.0)/CLOCKS_PER_SEC;
	return ((clock2-clock1))/(double)CLOCKS_PER_SEC;
	//return clock2 - clock1;
}


double mclock(){
	return clock();
	//timeval tim;
	//gettimeofday(&tim, NULL);
	//return tim.tv_sec + (tim.tv_usec / 1000000.0);
}

int init_paramsC0nj(type_paramsC0nj* pars_out, params pars_in) {
	(*pars_out).a = pars_in.a;
	(*pars_out).k0 = pars_in.k0;
	(*pars_out).alpha = pars_in.alpha;
	(*pars_out).z1 = pars_in.z1;
	(*pars_out).z2 = pars_in.z2;
	(*pars_out).A1 = pars_in.A1;
	(*pars_out).w1 = pars_in.w1;
	(*pars_out).epsilon0 = pars_in.epsilon0;
	(*pars_out).epsilon1 = pars_in.epsilon1;
	(*pars_out).epsilon2 = pars_in.epsilon2;
	(*pars_out).layer_type = pars_in.layer_type;
	(*pars_out).fg = pars_in.fg;
	(*pars_out).z = pars_in.z;
	return 0;
}

int init_paramsEps(type_paramsEps* pars_out, type_paramsC0nj pars_in) {
	(*pars_out).z1 = pars_in.z1;
	(*pars_out).z2 = pars_in.z2;
	(*pars_out).A1 = pars_in.A1;
	(*pars_out).w1 = pars_in.w1;
	(*pars_out).epsilon0 = pars_in.epsilon0;
	(*pars_out).epsilon1 = pars_in.epsilon1;
	(*pars_out).epsilon2 = pars_in.epsilon2;
	(*pars_out).layer_type = pars_in.layer_type;
	(*pars_out).fg = pars_in.fg;
	return 0;
}

int print_params(params exp0) {
	if (textout >= 1) {
		cout << "alpha" << endl << ">>> "; cout << exp0.alpha; cout << endl;
		cout << "lambda" << endl << ">>> "; cout << exp0.lambda; cout << endl;
		cout << "epsilon0" << endl << ">>> "; println_gsl_complex(exp0.epsilon0); cout << endl;
		cout << "epsilon1" << endl << ">>> "; println_gsl_complex(exp0.epsilon1); cout << endl;
		cout << "N1" << endl << ">>> " << exp0.N1 << endl << endl;
		cout << "N2" << endl << ">>> " << exp0.N2 << endl << endl;
		cout << "N" << endl << ">>> " << exp0.N << endl << endl;
	}

	cout << "A1" << endl << ">>> " << exp0.fg.A1 << endl << endl;

	return 0;
}

int println_gsl_complex(gsl_complex a) {
  std::cout << GSL_REAL(a)<<" + "<<GSL_IMAG(a)<<"*i"<<std::endl;
  return 0;
}

int print_gsl_complex(gsl_complex a) {
	std::cout << GSL_REAL(a) << " + " << GSL_IMAG(a) << "*i";
	return 0;
}

double absf(double x){
	if( x >= 0.0 )
	  return x;
	else
	  return -x;
}

int dep_params_calc(params* exp0) {
	if (textout >= 3) cout << "Dependent parameters calculation" << endl;
	(*exp0).k = 2 * M_PI / (*exp0).lambda; // wave number
	(*exp0).k0 = 2 * M_PI / (*exp0).lambda; // wave number
	(*exp0).epsilon0 = gsl_complex_rect((*exp0).epsilon0_re, (*exp0).epsilon0_im);
	(*exp0).epsilon1 = gsl_complex_rect((*exp0).epsilon1_re, (*exp0).epsilon1_im);

	(*exp0).N1 = floor(-(*exp0).k*(1 + sin((*exp0).alpha))) + 1;
	(*exp0).N2 = ceil((*exp0).k*(1 - sin((*exp0).alpha))) - 1;
	(*exp0).N = (*exp0).N2 - (*exp0).N1 + 1;

	(*exp0).zmin = (*exp0).fg.D0 - 0.1 - (absf((*exp0).fg.C1) + absf((*exp0).fg.C2) + absf((*exp0).fg.C3) + absf((*exp0).fg.D1) + absf((*exp0).fg.D2) + absf((*exp0).fg.D3)); // start of calculation domain
	(*exp0).zmax = (*exp0).fg.B0 + 0.1 + (absf((*exp0).fg.A1) + absf((*exp0).fg.A2) + absf((*exp0).fg.A3) + absf((*exp0).fg.B1) + absf((*exp0).fg.B2) + absf((*exp0).fg.B3));; // end of calculation domain
	(*exp0).z1 = (*exp0).zmin;
	(*exp0).z2 = (*exp0).zmax;

	if (textout >= 3) cout << "Dependent parameters calculation 2" << endl;

	return 0;
}




int load_data(params* pars) {
    using namespace std;

	params exp0;

	exp0.lambda = 2*M_PI; // wavelength
	exp0.alpha = 10.0*M_PI/180.0; // angle of plane wave direction
	exp0.a = 2 * M_PI; // period
	exp0.C0 = gsl_complex_exp(gsl_complex_rect(0.0,1.0)); // amplitude of the incedent wave
	exp0.epsilon0_re = 1.0; // inductive capacity for vacuum - dielectricheskaja pronicaemost'
	exp0.epsilon0_im = 0.0; // inductive capacity for vacuum - dielectricheskaja pronicaemost'
	exp0.mu0 = 1.0; // magnitic inductivity for vacuum - magnitnaja pronicaemost'
	exp0.zmin = -0.5; // start of calculation domain
	exp0.zmax = 4.5; // end of calculation domain

	// \varepsilon(x,z)
	//exp0.epsN = 3; // type of periodic structure 1 - layer, 2 - sin, 3 - Fourie
	exp0.epsilon1_re = 3.0; // inductive capacity for the periodic structure - real part
	exp0.epsilon1_im = 5.0; // inductive capacity for the periodic structure - complex part
	// type 1 - plane homogeneous layer
	// type 2 - [z1, z2 - A + A*sin(w*x)] homogeneous layer
	exp0.layer_type = 3;
	exp0.z1 = 0.0; // start of layer
	exp0.z2 = 1.0; // end of layer
	exp0.A1 = 1.0;
	exp0.w1 = 1.0;
	// type 3 - Fourie sum of 3 sin and 3 cos
	exp0.fg.B0 = 3.0;
	exp0.fg.D0 = 0.0;
	exp0.fg.i1 = 1.0;
	exp0.fg.i2 = 0.0;
	exp0.fg.i3 = 0.0;
	exp0.fg.A1 = 1.0;
	exp0.fg.A2 = 0.0;
	exp0.fg.A3 = 0.0;
	exp0.fg.B1 = 0.0;
	exp0.fg.B2 = 0.0;
	exp0.fg.B3 = 0.0;
	exp0.fg.C1 = 0.0;
	exp0.fg.C2 = 0.0;
	exp0.fg.C3 = 0.0;
	exp0.fg.D1 = 0.0;
	exp0.fg.D2 = 0.0;
	exp0.fg.D3 = 0.0;

	exp0.epsilon2 = gsl_complex_rect( 4.0, 4.0 );

	exp0.calcnum = 0;

	exp0.Nz = 7000;
	exp0.Nx = 200;

	// DEPENDENT PARAMETERS
	dep_params_calc(&exp0);

	if(textout >= 2){
      cout<<"exp0.N1"<<endl<<">>> "<<exp0.N1<<endl<<endl;
      cout<<"exp0.N2"<<endl<<">>> "<<exp0.N2<<endl<<endl;
    }

	*pars = exp0;

	return 0;
}

gsl_complex epsilon(double x, double z, void * pars){
  type_paramsEps par = *(type_paramsEps *)pars;

  //cout << "i1 " << par.fg.i1;

  if (par.layer_type == 1) {
	  if ((par.z1 <= z) && (z <= par.z2))
		  return par.epsilon1;
	  else
		  return par.epsilon0;
  } else if (par.layer_type == 2) {
	  if ((par.z1 <= z) && (z <= par.z2 - par.A1 + par.A1*sin(par.w1*x)))
		  return par.epsilon1;
	  else
		  return par.epsilon0;
  } else if (par.layer_type == 3) {
	  //cout << "layer3 ";
	  //cout << "A1 " << par.fg.A1;
	  //cout << "i1 " << par.fg.i1 << endl;
	  if ((par.fg.D0 + par.fg.C1*sin(par.fg.j1*x) + par.fg.D1*cos(par.fg.j1*x) + par.fg.C2*sin(par.fg.j2*x) + par.fg.D2*cos(par.fg.j2*x) <= z) && (z <= par.fg.B0 + par.fg.A1*sin(par.fg.i1*x) + par.fg.B1*cos(par.fg.i1*x) + par.fg.A2*sin(par.fg.i2*x) + par.fg.B2*cos(par.fg.i2*x)))		  
		  return par.epsilon1;
	  else
		  return par.epsilon0;
  } else if (par.layer_type == 4) {
	  if ((par.fg.D0 + par.fg.C1*sin(par.fg.j1*x) + par.fg.D1*cos(par.fg.j1*x) + par.fg.C2*sin(par.fg.j2*x) + par.fg.D2*cos(par.fg.j2*x) <= z) && (z <= par.fg.B0 + par.fg.A1*sin(par.fg.i1*x) + par.fg.B1*cos(par.fg.i1*x) + par.fg.A2*sin(par.fg.i2*x) + par.fg.B2*cos(par.fg.i2*x))){
		  if ((x - par.fg.cylX)*(x - par.fg.cylX) + (z - par.fg.cylZ)*(z - par.fg.cylZ) < par.fg.cylR*par.fg.cylR){
			  return par.epsilon2;
		  }
		  return par.epsilon1;
	  }
	  else
		  return par.epsilon0;
  }


}

double lambda_n(int n,params par) {
	double t = par.k0*par.a*sin(par.alpha);
	return (t + 2 * M_PI*n) / par.a;
}

gsl_complex psi_n(double x, int n, void * pars) {
	type_paramsC0nj par = *(type_paramsC0nj *)pars;
	double t = par.k0*par.a*sin(par.alpha);
	double lambda_n = (t + 2 * M_PI*n) / par.a;
	return gsl_complex_mul(gsl_complex_rect(1.0 / sqrt(par.a), 0.0), gsl_complex_exp(gsl_complex_rect(0.0, lambda_n*x)));
}

gsl_complex integrantC0nj(double x, void * pars) {
	type_paramsC0nj parC0 = *(type_paramsC0nj *)pars;
	type_paramsEps parsEps;
	init_paramsEps(&parsEps, parC0);
	//println_gsl_complex(epsilon(x, parC0.z, &parsEps));
	return gsl_complex_mul(epsilon(x, parC0.z, &parsEps), gsl_complex_mul(psi_n(x, parC0.n, pars), gsl_complex_conjugate(psi_n(x, parC0.j, pars))));
}

double integrantC0nj_re(double y, void * pars) {
	return GSL_REAL(integrantC0nj(y, pars));
}

double integrantC0nj_img(double y, void * pars) {
	return GSL_IMAG(integrantC0nj(y, pars));
}


gsl_complex integrantD2nj(double x, void * pars) {
	type_paramsC0nj parC0 = *(type_paramsC0nj *)pars;
	type_paramsEps parsEps;
	init_paramsEps(&parsEps, parC0);
	//println_gsl_complex(epsilon(x, parC0.z, &parsEps));
	return gsl_complex_mul(gsl_complex_inverse(epsilon(x, parC0.z, &parsEps)), gsl_complex_mul(psi_n(x, parC0.n, pars), gsl_complex_conjugate(psi_n(x, parC0.j, pars))));
}

double integrantD2nj_re(double y, void * pars) {
	return GSL_REAL(integrantD2nj(y, pars));
}

double integrantD2nj_img(double y, void * pars) {
	return GSL_IMAG(integrantD2nj(y, pars));
}


int calculate_Cnj(gsl_spline *** Cnj_re, gsl_spline *** Cnj_im, params* exp0){
	using namespace std;

	int N = (*exp0).N;
	int N1 = (*exp0).N1;
	int N2 = (*exp0).N2;

	double z;
	int Nz = (*exp0).Nz;
	double xi, yi, x[Nz], y1[Nz], y2[Nz];

	*Cnj_re = (gsl_spline**)malloc(sizeof(gsl_spline*)*(N*N));
	*Cnj_im = (gsl_spline**)malloc(sizeof(gsl_spline*)*(N*N));

	gsl_integration_workspace * w = gsl_integration_workspace_alloc(2000);
	gsl_spline *spline = gsl_spline_alloc(gsl_interp_akima, Nz);

	gsl_complex pI = gsl_complex_rect(0.0, 1.0);
	gsl_complex mI = gsl_complex_rect(0.0, -1.0);

	double result_re, result_img, error;
	type_paramsC0nj parC0nj;
	init_paramsC0nj(&parC0nj, *exp0);
	gsl_function F;
	size_t ww;

	for(int n = N1; n <= N2; n++) {
		for (int j = N1; j <= N2; j++) {

			//printf("\nn = %d  j = %d\n", n,j);

			parC0nj.n = n;
			parC0nj.j = j;

			for (int i = 0; i < (*exp0).Nz; i++) {
				z = (*exp0).zmin + ((*exp0).zmax - (*exp0).zmin)*i / ((*exp0).Nz - 1);
				parC0nj.z = z;

				x[i] = z;

				//printf("period a       = % .18f\n", parC0nj.a);

				F.function = &integrantC0nj_re;
				F.params = &parC0nj;
				//gsl_integration_qng(&F, 0, parC0nj.a, 1e-1, 1e-1, &result_re, &error, &ww);
				gsl_integration_qags(&F, 0, parC0nj.a, 1e-5, 1e-5, 2000, w, &result_re, &error);

				//printf("result RE       = % .18f\n", result_re);
				//printf("estimated error = % .18f\n", error);
				//printf("intervals =  %d\n", w->size);

				F.function = &integrantC0nj_img;
				//gsl_integration_qng(&F, 0, parC0nj.a, 1e-1, 1e-1, &result_img, &error, &ww);
				gsl_integration_qags(&F, 0, parC0nj.a, 1e-5, 1e-5, 2000, w, &result_img, &error);

				//printf("result IMG      = % .18f\n", result_img);
				//printf("estimated error = % .18f\n", error);
				//printf("intervals =  %d\n", w->size);

				gsl_complex int_res = gsl_complex_rect(result_re, result_img);

				if (textout >= 2) {
					//printf("point z       = % .18f\n", z);
					//cout << "Integral = " << GSL_REAL(int_res) << " + " << GSL_IMAG(int_res) << "*i" << endl << endl;
				}

				y1[i] = result_re;
				y1[i] *= (*exp0).k0*(*exp0).k0;
				if (n == j) y1[i] -= lambda_n(n,*exp0)*lambda_n(n, *exp0);

				y2[i] = result_img;
				y2[i] *= (*exp0).k0*(*exp0).k0;
				//if (n == j) y2[i] -= lambda_n(n, *exp0)*lambda_n(n, *exp0);
			}

			(*Cnj_re)[(n - N1)*N + (j - N1)] = gsl_spline_alloc(gsl_interp_akima, Nz);
			gsl_spline_init((*Cnj_re)[(n - N1)*N + (j - N1)], x, y1, Nz);
			(*Cnj_im)[(n - N1)*N + (j - N1)] = gsl_spline_alloc(gsl_interp_akima, Nz);
			gsl_spline_init((*Cnj_im)[(n - N1)*N + (j - N1)], x, y2, Nz);

			//(*Cnj)[(n-N1)*N+(j-N1)] = spline;

		}
	}

	//gsl_spline_free(spline);
	gsl_integration_workspace_free(w);

	/*
	gsl_interp_accel *acc = gsl_interp_accel_alloc();
	for (double xi = (*exp0).zmin; xi < (*exp0).zmax; xi += 0.01)
	{
		double yi = gsl_spline_eval((*Cnj)[0], xi, acc);
		printf("%g %g\n", xi, yi);
	}
	gsl_interp_accel_free(acc);
	*/

	return 0;
}


int calculate_C1nj(gsl_spline *** Cnj_re, gsl_spline *** Cnj_im, params* exp0){
	using namespace std;

//	cout<<endl<<" UNDER CONSTRUCTION === C1 === E_POLARIZATION "<<endl;
//	cout<<      " UNDER CONSTRUCTION === C1 === E_POLARIZATION "<<endl;
//	cout<<      " UNDER CONSTRUCTION === C1 === E_POLARIZATION "<<endl<<endl;
//	return 0;

	int N = (*exp0).N;
	int N1 = (*exp0).N1;
	int N2 = (*exp0).N2;

	double z;
	int Nz = (*exp0).Nz;
	double xi, yi, x[Nz], y1[Nz], y2[Nz];

	*Cnj_re = (gsl_spline**)malloc(sizeof(gsl_spline*)*(N*N));
	*Cnj_im = (gsl_spline**)malloc(sizeof(gsl_spline*)*(N*N));

	gsl_integration_workspace * w = gsl_integration_workspace_alloc(2000);
	gsl_spline *spline = gsl_spline_alloc(gsl_interp_akima, Nz);

	gsl_complex pI = gsl_complex_rect(0.0, 1.0);
	gsl_complex mI = gsl_complex_rect(0.0, -1.0);

	double result_re, result_img, error;
	type_paramsC0nj parC0nj;
	init_paramsC0nj(&parC0nj, *exp0);
	gsl_function F;
	size_t ww;

	for(int n = N1; n <= N2; n++) {
		for (int j = N1; j <= N2; j++) {

			//printf("\nn = %d  j = %d\n", n,j);

			parC0nj.n = n;
			parC0nj.j = j;

			for (int i = 0; i < (*exp0).Nz; i++) {
				z = (*exp0).zmin + ((*exp0).zmax - (*exp0).zmin)*i / ((*exp0).Nz - 1);
				parC0nj.z = z;

				x[i] = z;

				//printf("period a       = % .18f\n", parC0nj.a);

				F.function = &integrantD2nj_re;
				F.params = &parC0nj;
				//gsl_integration_qng(&F, 0, parC0nj.a, 1e-1, 1e-1, &result_re, &error, &ww);
				gsl_integration_qags(&F, 0, parC0nj.a, 1e-5, 1e-5, 2000, w, &result_re, &error);

				//printf("result RE       = % .18f\n", result_re);
				//printf("estimated error = % .18f\n", error);
				//printf("intervals =  %d\n", w->size);

				F.function = &integrantD2nj_img;
				//gsl_integration_qng(&F, 0, parC0nj.a, 1e-1, 1e-1, &result_img, &error, &ww);
				gsl_integration_qags(&F, 0, parC0nj.a, 1e-5, 1e-5, 2000, w, &result_img, &error);

				//printf("result IMG      = % .18f\n", result_img);
				//printf("estimated error = % .18f\n", error);
				//printf("intervals =  %d\n", w->size);

				gsl_complex int_res = gsl_complex_rect(result_re, result_img);

				if (textout >= 2) {
					//printf("point z       = % .18f\n", z);
					//cout << "Integral = " << GSL_REAL(int_res) << " + " << GSL_IMAG(int_res) << "*i" << endl << endl;
				}

				y1[i] = result_re;
				y1[i] *= lambda_n(n,*exp0)*lambda_n(j,*exp0);
				//if (n == j) y1[i] -= lambda_n(n,*exp0)*lambda_n(n, *exp0);
				if (n == j) y1[i] -= (*exp0).k0;

				y2[i] = result_img;
				y2[i] *= lambda_n(n,*exp0)*lambda_n(j,*exp0);

				//cout<<"DEBUG result imag = " << result_img << endl;

				//if (n == j) y2[i] -= lambda_n(n, *exp0)*lambda_n(n, *exp0);
			}

			(*Cnj_re)[(n - N1)*N + (j - N1)] = gsl_spline_alloc(gsl_interp_akima, Nz);
			gsl_spline_init((*Cnj_re)[(n - N1)*N + (j - N1)], x, y1, Nz);
			(*Cnj_im)[(n - N1)*N + (j - N1)] = gsl_spline_alloc(gsl_interp_akima, Nz);
			gsl_spline_init((*Cnj_im)[(n - N1)*N + (j - N1)], x, y2, Nz);

			//(*Cnj)[(n-N1)*N+(j-N1)] = spline;

		}
	}

	//gsl_spline_free(spline);
	gsl_integration_workspace_free(w);

	/*
	gsl_interp_accel *acc = gsl_interp_accel_alloc();
	for (double xi = (*exp0).zmin; xi < (*exp0).zmax; xi += 0.01)
	{
		double yi = gsl_spline_eval((*Cnj)[0], xi, acc);
		printf("%g %g\n", xi, yi);
	}
	gsl_interp_accel_free(acc);
	*/

	return 0;
}


int calculate_C2nj(gsl_spline *** Cnj_re, gsl_spline *** Cnj_im, params* exp0){
	using namespace std;

	int N = (*exp0).N;
	int N1 = (*exp0).N1;
	int N2 = (*exp0).N2;

	double z;
	int Nz = (*exp0).Nz;
	double xi, yi, x[Nz], y1[Nz], y2[Nz];
	double D2nj_y1[N*N], D2nj_y2[N*N], C2nj_ati_y1[N*N*Nz], C2nj_ati_y2[N*N*Nz];

	*Cnj_re = (gsl_spline**)malloc(sizeof(gsl_spline*)*(N*N));
	*Cnj_im = (gsl_spline**)malloc(sizeof(gsl_spline*)*(N*N));

	gsl_integration_workspace * w = gsl_integration_workspace_alloc(2000);
	gsl_spline *spline = gsl_spline_alloc(gsl_interp_akima, Nz);

	gsl_complex pI = gsl_complex_rect(0.0, 1.0);
	gsl_complex mI = gsl_complex_rect(0.0, -1.0);

	double result_re, result_img, error;
	type_paramsC0nj parC0nj;
	init_paramsC0nj(&parC0nj, *exp0);
	gsl_function F;
	size_t ww;


	for (int i = 0; i < (*exp0).Nz; i++) {
		z = (*exp0).zmin + ((*exp0).zmax - (*exp0).zmin)*i / ((*exp0).Nz - 1);
		parC0nj.z = z;

		x[i] = z;

	for(int n = N1; n <= N2; n++) {
		for (int j = N1; j <= N2; j++) {

			//printf("\nn = %d  j = %d\n", n,j);

			parC0nj.n = n;
			parC0nj.j = j;



				//printf("period a       = % .18f\n", parC0nj.a);

				F.function = &integrantD2nj_re;
				F.params = &parC0nj;
				//gsl_integration_qng(&F, 0, parC0nj.a, 1e-1, 1e-1, &result_re, &error, &ww);
				gsl_integration_qags(&F, 0, parC0nj.a, 1e-5, 1e-5, 2000, w, &result_re, &error);

				//printf("result RE       = % .18f\n", result_re);
				//printf("estimated error = % .18f\n", error);
				//printf("intervals =  %d\n", w->size);

				F.function = &integrantD2nj_img;
				//gsl_integration_qng(&F, 0, parC0nj.a, 1e-1, 1e-1, &result_img, &error, &ww);
				gsl_integration_qags(&F, 0, parC0nj.a, 1e-5, 1e-5, 2000, w, &result_img, &error);

				//printf("result IMG      = % .18f\n", result_img);
				//printf("estimated error = % .18f\n", error);
				//printf("intervals =  %d\n", w->size);

				gsl_complex int_res = gsl_complex_rect(result_re, result_img);

				if (textout >= 2) {
					//printf("point z       = % .18f\n", z);
					//cout << "Integral = " << GSL_REAL(int_res) << " + " << GSL_IMAG(int_res) << "*i" << endl << endl;
				}

				D2nj_y1[(n - N1)*N + (j - N1)] = result_re;
				//y1[i] *= (*exp0).k0*(*exp0).k0;
				//if (n == j) y1[i] -= lambda_n(n,*exp0)*lambda_n(n, *exp0);

				D2nj_y2[(n - N1)*N + (j - N1)] = result_img;
				//y2[i] *= (*exp0).k0*(*exp0).k0;
				//if (n == j) y2[i] -= lambda_n(n, *exp0)*lambda_n(n, *exp0);
			}

			//(*Cnj_re)[(n - N1)*N + (j - N1)] = gsl_spline_alloc(gsl_interp_akima, Nz);
			//gsl_spline_init((*Cnj_re)[(n - N1)*N + (j - N1)], x, y1, Nz);
			//(*Cnj_im)[(n - N1)*N + (j - N1)] = gsl_spline_alloc(gsl_interp_akima, Nz);
			//gsl_spline_init((*Cnj_im)[(n - N1)*N + (j - N1)], x, y2, Nz);

			//(*Cnj)[(n-N1)*N+(j-N1)] = spline;

		}

	    //
	    // Inverse at point [i]
	    //

	//
	// Inverse D2 matrix
	//

	int s;
	gsl_permutation *p=gsl_permutation_alloc(N);
	gsl_matrix_complex *es=gsl_matrix_complex_alloc(N,N);
	gsl_matrix_complex *esinv=gsl_matrix_complex_alloc(N,N);

	for(int n = N1; n <= N2; n++) {
	  for (int j = N1; j <= N2; j++) {
		gsl_matrix_complex_set( es, n-N1, j-N1, gsl_complex_rect( D2nj_y1[(n - N1)*N + (j - N1)], D2nj_y2[(n - N1)*N + (j - N1)] ) );
	  }
    }

	//for(int ii=0; ii < N; ii++){
	//    for(int jj=0; jj < N; jj++){
	//       	printf("\nes(%d,%d)=%g+%g*j",ii,jj,GSL_REAL(gsl_matrix_complex_get(es,ii,jj)),GSL_IMAG(gsl_matrix_complex_get(es,ii,jj)));
	//    }
	//}
	//cout << endl;

	gsl_linalg_complex_LU_decomp(es,p,&s);
	gsl_linalg_complex_LU_invert(es,p,esinv);

	for(int ii=0; ii < N; ii++){
	    for(int jj=0; jj < N; jj++){
        	//printf("\nesinv(%d,%d)=%g+%g*j",ii,jj,GSL_REAL(gsl_matrix_complex_get(esinv,ii,jj)),GSL_IMAG(gsl_matrix_complex_get(esinv,ii,jj)));
        	D2nj_y1[(ii)*N + (jj)] = GSL_REAL(gsl_matrix_complex_get(esinv,ii,jj));
        	D2nj_y2[(ii)*N + (jj)] = GSL_IMAG(gsl_matrix_complex_get(esinv,ii,jj));
	    }
	}

	    // IMPORTANT note:
	    // at this moment arrays D2nj_y1[], D2nj_y2[] should contain the matrix C2 = D2^-1
	    //
  	    for(int n = N1; n <= N2; n++) {
		    for (int j = N1; j <= N2; j++) {
	          C2nj_ati_y1[Nz*((n - N1)*N + (j - N1)) + i] = D2nj_y1[(n - N1)*N + (j - N1)];
	          C2nj_ati_y2[Nz*((n - N1)*N + (j - N1)) + i] = D2nj_y2[(n - N1)*N + (j - N1)];
		    }
	    }

	}


	//
	// Spline Interpolation
	//
	for(int n = N1; n <= N2; n++) {
		for (int j = N1; j <= N2; j++) {

			(*Cnj_re)[(n - N1)*N + (j - N1)] = gsl_spline_alloc(gsl_interp_akima, Nz);
			gsl_spline_init((*Cnj_re)[(n - N1)*N + (j - N1)], x, C2nj_ati_y1 + (((n - N1)*N + (j - N1))*Nz), Nz);
			(*Cnj_im)[(n - N1)*N + (j - N1)] = gsl_spline_alloc(gsl_interp_akima, Nz);
			gsl_spline_init((*Cnj_im)[(n - N1)*N + (j - N1)], x, C2nj_ati_y2 + (((n - N1)*N + (j - N1))*Nz), Nz);

		}
	}


	//gsl_spline_free(spline);
	gsl_integration_workspace_free(w);

	/*
	gsl_interp_accel *acc = gsl_interp_accel_alloc();
	for (double xi = (*exp0).zmin; xi < (*exp0).zmax; xi += 0.01)
	{
		double yi = gsl_spline_eval((*Cnj)[0], xi, acc);
		printf("%g %g\n", xi, yi);
	}
	gsl_interp_accel_free(acc);
	*/

//	delete[] D2nj_y1;
//	delete[] D2nj_y2;
//	delete[] C2nj_ati_y1;
//	delete[] C2nj_ati_y2;


	//return 0;

	return 0;
}


int calculate_D2nj(gsl_spline *** Cnj_re, gsl_spline *** Cnj_im, params* exp0){
	using namespace std;

//	cout<<endl<<" UNDER CONSTRUCTION === D2 === E_POLARIZATION "<<endl;
//	cout<<      " UNDER CONSTRUCTION === D2 === E_POLARIZATION "<<endl;
//	cout<<      " UNDER CONSTRUCTION === D2 === E_POLARIZATION "<<endl<<endl;
//	return 0;

	int N = (*exp0).N;
	int N1 = (*exp0).N1;
	int N2 = (*exp0).N2;

	double z;
	int Nz = (*exp0).Nz;
	double xi, yi, x[Nz], y1[Nz], y2[Nz];

	*Cnj_re = (gsl_spline**)malloc(sizeof(gsl_spline*)*(N*N));
	*Cnj_im = (gsl_spline**)malloc(sizeof(gsl_spline*)*(N*N));

	gsl_integration_workspace * w = gsl_integration_workspace_alloc(2000);
	gsl_spline *spline = gsl_spline_alloc(gsl_interp_akima, Nz);

	gsl_complex pI = gsl_complex_rect(0.0, 1.0);
	gsl_complex mI = gsl_complex_rect(0.0, -1.0);

	double result_re, result_img, error;
	type_paramsC0nj parC0nj;
	init_paramsC0nj(&parC0nj, *exp0);
	gsl_function F;
	size_t ww;

	for(int n = N1; n <= N2; n++) {
		for (int j = N1; j <= N2; j++) {

			//printf("\nn = %d  j = %d\n", n,j);

			parC0nj.n = n;
			parC0nj.j = j;

			for (int i = 0; i < (*exp0).Nz; i++) {
				z = (*exp0).zmin + ((*exp0).zmax - (*exp0).zmin)*i / ((*exp0).Nz - 1);
				parC0nj.z = z;

				x[i] = z;

				//printf("period a       = % .18f\n", parC0nj.a);

				F.function = &integrantD2nj_re;
				F.params = &parC0nj;
				//gsl_integration_qng(&F, 0, parC0nj.a, 1e-1, 1e-1, &result_re, &error, &ww);
				gsl_integration_qags(&F, 0, parC0nj.a, 1e-5, 1e-5, 2000, w, &result_re, &error);

				//printf("result RE       = % .18f\n", result_re);
				//printf("estimated error = % .18f\n", error);
				//printf("intervals =  %d\n", w->size);

				F.function = &integrantD2nj_img;
				//gsl_integration_qng(&F, 0, parC0nj.a, 1e-1, 1e-1, &result_img, &error, &ww);
				gsl_integration_qags(&F, 0, parC0nj.a, 1e-5, 1e-5, 2000, w, &result_img, &error);

				//printf("result IMG      = % .18f\n", result_img);
				//printf("estimated error = % .18f\n", error);
				//printf("intervals =  %d\n", w->size);

				gsl_complex int_res = gsl_complex_rect(result_re, result_img);

				if (textout >= 2) {
					//printf("point z       = % .18f\n", z);
					//cout << "Integral = " << GSL_REAL(int_res) << " + " << GSL_IMAG(int_res) << "*i" << endl << endl;
				}

				y1[i] = result_re;
				//y1[i] *= (*exp0).k0*(*exp0).k0;
				//if (n == j) y1[i] -= lambda_n(n,*exp0)*lambda_n(n, *exp0);

				y2[i] = result_img;
				//y2[i] *= (*exp0).k0*(*exp0).k0;
				//if (n == j) y2[i] -= lambda_n(n, *exp0)*lambda_n(n, *exp0);
			}

			(*Cnj_re)[(n - N1)*N + (j - N1)] = gsl_spline_alloc(gsl_interp_akima, Nz);
			gsl_spline_init((*Cnj_re)[(n - N1)*N + (j - N1)], x, y1, Nz);
			(*Cnj_im)[(n - N1)*N + (j - N1)] = gsl_spline_alloc(gsl_interp_akima, Nz);
			gsl_spline_init((*Cnj_im)[(n - N1)*N + (j - N1)], x, y2, Nz);

			//(*Cnj)[(n-N1)*N+(j-N1)] = spline;

		}
	}

	//gsl_spline_free(spline);
	gsl_integration_workspace_free(w);

	/*
	gsl_interp_accel *acc = gsl_interp_accel_alloc();
	for (double xi = (*exp0).zmin; xi < (*exp0).zmax; xi += 0.01)
	{
		double yi = gsl_spline_eval((*Cnj)[0], xi, acc);
		printf("%g %g\n", xi, yi);
	}
	gsl_interp_accel_free(acc);
	*/

	return 0;
}


int func(double z, const double y[], double f[], void *pars)
{
	//double mu = *(double *)params;
	params par = *(params *)pars;

	int N = par.N;

	gsl_interp_accel *acc = gsl_interp_accel_alloc();

	for(int j = 0; j < N; j++) {

        // {Q_1,..Q_N}^T = (0|E)Q
		f[j*2] = y[(j+N)*2];
		f[j * 2+1] = y[(j + N) * 2+1];

		// {Q_N+1,..Q_2N}^T = (G|0)Q
		double re_temp = 0.0, im_temp = 0.0;
		for (int n = 0; n < N; n++) {
			double CnjRE = gsl_spline_eval(par.Cnj_re[n*N + j], z, acc);
			double CnjIM = gsl_spline_eval(par.Cnj_im[n*N + j], z, acc);
			re_temp += -CnjRE*y[n * 2] + CnjIM*y[n * 2 + 1];
			im_temp += -CnjIM*y[n * 2] - CnjRE*y[n * 2 + 1];
		}
		f[(j+N) * 2] = re_temp;
		f[(j + N) * 2 + 1] = im_temp;
	}

	gsl_interp_accel_free(acc);

	return GSL_SUCCESS;
}


int funcE(double z, const double y[], double f[], void *pars)
{
	//double mu = *(double *)params;
	params par = *(params *)pars;

	int N = par.N;
	double re_temp = 0.0, im_temp = 0.0;

	gsl_interp_accel *acc = gsl_interp_accel_alloc();

	for(int j = 0; j < N; j++) {

	    //
	    // H-pol:
        // Q = (  0 | E )Q
        //     ( -C | 0 )
        //
        // E-pol:
        // Q = ( 0  | C1 )Q
        //     ( C2 | 0  )
        //
		//f[j*2] = y[(j+N)*2];
		//f[j * 2+1] = y[(j + N) * 2+1];

		re_temp = 0.0, im_temp = 0.0;
		for (int n = 0; n < N; n++) {
			double CnjRE = gsl_spline_eval(par.C1nj_re[n*N + j], z, acc);
			double CnjIM = gsl_spline_eval(par.C1nj_im[n*N + j], z, acc);
			re_temp += CnjRE*y[(n) * 2] - CnjIM*y[(n) * 2 + 1];
			im_temp += CnjIM*y[(n) * 2] + CnjRE*y[(n) * 2 + 1];			
		}
		f[(j + N) * 2] = re_temp;
		f[(j + N) * 2 + 1] = im_temp;

		re_temp = 0.0, im_temp = 0.0;
		for (int n = 0; n < N; n++) {
			double CnjRE = gsl_spline_eval(par.C2nj_re[n*N + j], z, acc);
			double CnjIM = gsl_spline_eval(par.C2nj_im[n*N + j], z, acc);
			re_temp += CnjRE*y[(n + N) * 2] - CnjIM*y[(n + N) * 2 + 1];
			im_temp += CnjIM*y[(n + N) * 2] + CnjRE*y[(n + N) * 2 + 1];
		}
		f[(j + 0) * 2] = re_temp;
		f[(j + 0) * 2 + 1] = im_temp;
	}

	gsl_interp_accel_free(acc);

	return GSL_SUCCESS;
}


int jac(double z, const double y[], double *dfdy, double dfdt[], void *pars){

	params par = *(params *)pars;

	int N = par.N;

	gsl_matrix_view dfdy_mat = gsl_matrix_view_array(dfdy, 4*N, 4*N);
	gsl_matrix * m = &dfdy_mat.matrix;
	gsl_matrix_set_zero(m);

	gsl_interp_accel *acc = gsl_interp_accel_alloc();

	for (int j = 0; j < N; j++) {
		gsl_matrix_set(m, j*2, N*2+j*2, 1.0);
		gsl_matrix_set(m, j * 2 + 1, N * 2 + j * 2 + 1, 1.0);

		for (int n = 0; n < N; n++) {
			double CnjRE = gsl_spline_eval(par.Cnj_re[n*N + j], z, acc);
			double CnjIM = gsl_spline_eval(par.Cnj_im[n*N + j], z, acc);
			gsl_matrix_set(m, j * 2 + 2 * N,     n * 2,     -CnjRE);
			gsl_matrix_set(m, j * 2 + 2 * N,     n * 2 + 1,  CnjIM );
			gsl_matrix_set(m, j * 2 + 2 * N + 1, n * 2 ,    -CnjIM);
			gsl_matrix_set(m, j * 2 + 2 * N + 1, n * 2 + 1, -CnjRE);
		}
	}

	gsl_interp_accel_free(acc);

	for(int j = 0; j < N*4; j++)
	  dfdt[j] = 0.0;

	return GSL_SUCCESS;
}


int jacE(double z, const double y[], double *dfdy, double dfdt[], void *pars){

	params par = *(params *)pars;

	int N = par.N;

	gsl_matrix_view dfdy_mat = gsl_matrix_view_array(dfdy, 4*N, 4*N);
	gsl_matrix * m = &dfdy_mat.matrix;
	gsl_matrix_set_zero(m);

	gsl_interp_accel *acc = gsl_interp_accel_alloc();

    //
    // H-pol:
    // Q = (  0 | E )Q
    //     ( -C | 0 )
    //
    // E-pol:
    // Q = ( 0  | C1 )Q
    //     ( C2 | 0  )
    //

	for (int j = 0; j < N; j++) {
		//gsl_matrix_set(m, j*2, N+j*2, 1.0);
		//gsl_matrix_set(m, j * 2+1, N + j * 2+1, 1.0);

		for (int n = 0; n < N; n++) {
			double CnjRE = gsl_spline_eval(par.C2nj_re[j*N + n], z, acc);
			double CnjIM = gsl_spline_eval(par.C2nj_im[j*N + n], z, acc);
			gsl_matrix_set( m, j * 2 + 2 * N,     n * 2,      CnjRE );
			gsl_matrix_set( m, j * 2 + 2 * N,     n * 2 + 1, -CnjIM );
			gsl_matrix_set( m, j * 2 + 2 * N + 1, n * 2,      CnjIM );
			gsl_matrix_set( m, j * 2 + 2 * N + 1, n * 2 + 1,  CnjRE );

			CnjRE = gsl_spline_eval(par.C1nj_re[j*N + n], z, acc);
			CnjIM = gsl_spline_eval(par.C1nj_im[j*N + n], z, acc);
			gsl_matrix_set(m, j * 2,     n * 2 + 2 * N,      CnjRE);
			gsl_matrix_set(m, j * 2,     n * 2 + 2 * N + 1,  -CnjIM);
			gsl_matrix_set(m, j * 2 + 1, n * 2 + 2 * N,      CnjIM);
			gsl_matrix_set(m, j * 2 + 1, n * 2 + 2 * N + 1,  CnjRE);

		}
	}

	gsl_interp_accel_free(acc);

	for(int j = 0; j < N*4; j++)
	  dfdt[j] = 0.0;

	return GSL_SUCCESS;
}


int solve_SODE_H(params* exp0, int s, gsl_complex* QsjArr, gsl_complex* Qsj_zmax) {

	int N = (*exp0).N;
	int Nz = (*exp0).Nz;
	double zarr[Nz];
    // array with all solution components Qj(z) for s-th initial values
    // QsjArr[Nz*j], ..., QsjArr[Nz*j+Nz-1] - values for function Qj

	//for(int j = 0; j < N * 2; j++)
	//  Qsj[j] = gsl_spline_alloc(gsl_interp_akima, Nz);

	double dz = ((*exp0).zmax - (*exp0).zmin) / (Nz - 1);
	gsl_odeiv2_system sys = { func, jac, 2*N*2, exp0 };
	gsl_odeiv2_driver * d = gsl_odeiv2_driver_alloc_y_new(&sys, gsl_odeiv2_step_rk8pd, dz, 1e-6, 1e-6 );
	double z = (*exp0).zmin;
	double y[2*N*2];
	// 0 step
	zarr[0] = z;
	for (int i = 0; i < 4 * N; i++)
		y[i]=0.0;
	y[s * 2] = 1.0;
	for (int i = 0; i < 2 * N; i++)
		QsjArr[Nz*i + 0] = gsl_complex_rect(y[i * 2], y[i * 2 + 1]);
	// step evolving
	for(int j = 1; j < Nz; j++) {
		zarr[j] = (*exp0).zmin + j*dz;
		int status = gsl_odeiv2_driver_apply(d, &z, zarr[j], y);
		for(int i = 0; i < 2 * N; i++)
			QsjArr[Nz*i + j] = gsl_complex_rect(y[i * 2], y[i * 2 + 1]);
	}

	if (s == 0) {
		ofstream myfile;
		// Q01
		myfile.open("Q01_re.txt");
		for (int i = 0; i < Nz; i++)
			myfile << zarr[i] << " " << GSL_REAL(QsjArr[Nz * 0 + i]) << endl;
		myfile.close();
		myfile.open("Q01_im.txt");
		for (int i = 0; i < Nz; i++)
			myfile << zarr[i] << " " << GSL_IMAG(QsjArr[Nz * 0 + i]) << endl;
		myfile.close();
		// Q04
		myfile.open("Q04_re.txt");
		for (int i = 0; i < Nz; i++)
			myfile << zarr[i] << " " << GSL_REAL(QsjArr[Nz * 3 + i]) << endl;
		myfile.close();
		myfile.open("Q04_im.txt");
		for (int i = 0; i < Nz; i++)
			myfile << zarr[i] << " " << GSL_IMAG(QsjArr[Nz * 3 + i]) << endl;
		myfile.close();
	}
	if (s == 1) {
		ofstream myfile;
		// Q12
		myfile.open("Q12_re.txt");
		for (int i = 0; i < Nz; i++)
			myfile << zarr[i] << " " << GSL_REAL(QsjArr[Nz * 1 + i]) << endl;
		myfile.close();
		myfile.open("Q12_im.txt");
		for (int i = 0; i < Nz; i++)
			myfile << zarr[i] << " " << GSL_IMAG(QsjArr[Nz * 1 + i]) << endl;
		myfile.close();
		// Q15
		myfile.open("Q15_re.txt");
		for (int i = 0; i < Nz; i++)
			myfile << zarr[i] << " " << GSL_REAL(QsjArr[Nz * 4 + i]) << endl;
		myfile.close();
		myfile.open("Q15_im.txt");
		for (int i = 0; i < Nz; i++)
			myfile << zarr[i] << " " << GSL_IMAG(QsjArr[Nz * 4 + i]) << endl;
		myfile.close();
	}

	for(int j = 0; j < N * 2; j++) {
		Qsj_zmax[j] = QsjArr[Nz*j+Nz-1];
		//gsl_spline_init(Qsj[j], zarr, QsjArr+Nz*j, Nz);
	}

	return 0;
}


int solve_SODE_E(params* exp0, int s, gsl_complex* QsjArr, gsl_complex* Qsj_zmax) {

	int N = (*exp0).N;
	int Nz = (*exp0).Nz;
	double zarr[Nz];
    // array with all solution components Qj(z) for s-th initial values
    // QsjArr[Nz*j], ..., QsjArr[Nz*j+Nz-1] - values for function Qj

	//for(int j = 0; j < N * 2; j++)
	//  Qsj[j] = gsl_spline_alloc(gsl_interp_akima, Nz);

	double dz = ((*exp0).zmax - (*exp0).zmin) / (Nz - 1);
	gsl_odeiv2_system sys = { funcE, jacE, 2*N*2, exp0 };
	gsl_odeiv2_driver * d = gsl_odeiv2_driver_alloc_y_new(&sys, gsl_odeiv2_step_rk8pd, dz, 1e-6, 1e-6 );
	double z = (*exp0).zmin;
	double y[2*N*2];
	// 0 step
	zarr[0] = z;
	for (int i = 0; i < 4 * N; i++){
		y[i] = 0.0;
	}
	y[s * 2] = 1.0;
	for (int i = 0; i < 2 * N; i++){
		QsjArr[Nz*i + 0] = gsl_complex_rect(y[i * 2], y[i * 2 + 1]);
	}
	// step evolving
	for(int j = 1; j < Nz; j++) {
		zarr[j] = (*exp0).zmin + j*dz;
		int status = gsl_odeiv2_driver_apply(d, &z, zarr[j], y);
		for(int i = 0; i < 2 * N; i++)
			QsjArr[Nz*i + j] = gsl_complex_rect(y[i * 2], y[i * 2 + 1]);
	}

	if (s == 0) {
		ofstream myfile;
		// Q01
		myfile.open("Q01_re.txt");
		for (int i = 0; i < Nz; i++)
			myfile << zarr[i] << " " << GSL_REAL(QsjArr[Nz * 0 + i]) << endl;
		myfile.close();
		myfile.open("Q01_im.txt");
		for (int i = 0; i < Nz; i++)
			myfile << zarr[i] << " " << GSL_IMAG(QsjArr[Nz * 0 + i]) << endl;
		myfile.close();
		// Q04
		myfile.open("Q04_re.txt");
		for (int i = 0; i < Nz; i++)
			myfile << zarr[i] << " " << GSL_REAL(QsjArr[Nz * 3 + i]) << endl;
		myfile.close();
		myfile.open("Q04_im.txt");
		for (int i = 0; i < Nz; i++)
			myfile << zarr[i] << " " << GSL_IMAG(QsjArr[Nz * 3 + i]) << endl;
		myfile.close();
		/*
		ofstream myfile;
		// B10_abs
		myfile.open("B10_abs.txt");
		for (int i = 0; i < Nz; i++)
	    myfile << gsl_complex_abs(QsjArr[Nz * 0 + i]) << endl;
		myfile.close();
		// B10_re
		myfile.open("B10_re.txt");
		for (int i = 0; i < Nz; i++)
			myfile << GSL_REAL(QsjArr[Nz * 0 + i]) << endl;
		myfile.close();
		// B10'_abs
		myfile.open("B10p_abs.txt");
		for (int i = 0; i < Nz; i++)
			myfile << gsl_complex_abs(QsjArr[Nz * (N + 0) + i]) << endl;
		myfile.close();
		// B10'_re
		myfile.open("B10p_re.txt");
		for (int i = 0; i < Nz; i++)
			myfile << GSL_REAL(QsjArr[Nz * (N + 0) + i]) << endl;
		myfile.close();
		*/
	}
	if (s == 1) {
		ofstream myfile;
		// B10_abs
		//myfile.open("B11_abs.txt");
		//for (int i = 0; i < Nz; i++)
		//	myfile << gsl_complex_abs(QsjArr[Nz * (N*s + ) + i]) << endl;
		//myfile.close();
		// Q12
		myfile.open("Q12_re.txt");
		for (int i = 0; i < Nz; i++)
			myfile << zarr[i] << " " << GSL_REAL(QsjArr[Nz * 1 + i]) << endl;
		myfile.close();
		myfile.open("Q12_im.txt");
		for (int i = 0; i < Nz; i++)
			myfile << zarr[i] << " " << GSL_IMAG(QsjArr[Nz * 1 + i]) << endl;
		myfile.close();
		// Q15
		myfile.open("Q15_re.txt");
		for (int i = 0; i < Nz; i++)
			myfile << zarr[i] << " " << GSL_REAL(QsjArr[Nz * 4 + i]) << endl;
		myfile.close();
		myfile.open("Q15_im.txt");
		for (int i = 0; i < Nz; i++)
			myfile << zarr[i] << " " << GSL_IMAG(QsjArr[Nz * 4 + i]) << endl;
		myfile.close();
		// B10'_abs
		//myfile.open("B11p_abs.txt");
		//for (int i = 0; i < Nz; i++)
		//	myfile << gsl_complex_abs(QsjArr[Nz * (N + 0) + i]) << endl;
		//myfile.close();
		// B10'_re
		//myfile.open("B11p_re.txt");
		//for (int i = 0; i < Nz; i++)
		//	myfile << GSL_REAL(QsjArr[Nz * (N + 0) + i]) << endl;
		//myfile.close();
	}

	for(int j = 0; j < N * 2; j++) {
		Qsj_zmax[j] = QsjArr[Nz*j+Nz-1];
		//gsl_spline_init(Qsj[j], zarr, QsjArr+Nz*j, Nz);
	}

    return 0;
}




int solve_SODE(params* exp0, int s, gsl_complex* QsjArr, gsl_complex* Qsj_zmax){

	if ((*exp0).polar == 1){
		return solve_SODE_H(exp0, s, QsjArr, Qsj_zmax);
	}else{
		return solve_SODE_E(exp0, s, QsjArr, Qsj_zmax);
	}
	return 1;
}


int solve_SLE_H(gsl_complex* alphas, gsl_complex* Qsj_zmax, params pars) {

	int N = pars.N;

	for (int i = 0; i < 2 * N; i++)
		alphas[i] = gsl_complex_rect(0.0, 0.0);

	gsl_matrix_complex * m = gsl_matrix_complex_alloc(2 * N, 2 * N);
	gsl_vector_complex * b = gsl_vector_complex_alloc(2 * N);
	gsl_vector_complex * x = gsl_vector_complex_alloc(2 * N);


	for (int j = 0; j < 2 * N; j++) {
		gsl_vector_complex_set(b, j, gsl_complex_rect(0.0, 0.0));
		for (int s = 0; s < 2 * N; s++)
			gsl_matrix_complex_set(m, j, s, gsl_complex_rect(0.0, 0.0));
	}

	gsl_complex RP, Mjs;
	for (int j = 0; j < N; j++) {

		double alpha_j = pars.k0*sin(pars.alpha) + j + pars.N1;
		double gamma_j = sqrt(pars.k0*pars.k0 - alpha_j*alpha_j);

		if (textout >= 3){
			cout << endl << "gamma_j" << endl << ">>> " << gamma_j << endl << endl;
		}



		RP = gsl_complex_mul(gsl_complex_mul(gsl_complex_rect(0.0, -2.0*gamma_j*sqrt(pars.a)), pars.C0), gsl_complex_exp(gsl_complex_rect(0.0, -gamma_j*pars.zmax)));

		if (textout >= 3){
			cout << endl << "RP" << endl << ">>> ";  print_gsl_complex(RP); cout << endl << endl;
			cout << endl << "RP1" << endl << ">>> ";  print_gsl_complex(pars.C0); cout << endl << endl;
		}

		if (j + pars.N1 == 0)
			gsl_vector_complex_set(b, N + j, RP);

		gsl_matrix_complex_set(m, j, j, gsl_complex_rect(0.0, gamma_j));
		gsl_matrix_complex_set(m, j, j + N, gsl_complex_rect(1.0, 0.0));
		for (int s = 0; s < 2 * N; s++) {
			Mjs = gsl_complex_add(Qsj_zmax[2 * N*s + j + N], gsl_complex_mul(gsl_complex_rect(0.0, -gamma_j), Qsj_zmax[2 * N*s + j]));
			gsl_matrix_complex_set(m, j + N, s, Mjs);
		}
	}

	if (textout >= 3){
		printf("m = \n");
		gsl_matrix_complex_fprintf(stdout, m, "%g");

		printf("\nb = \n");
		gsl_vector_complex_fprintf(stdout, b, "%g");
	}

	int s;

	gsl_permutation * p = gsl_permutation_alloc(2 * N);

	gsl_linalg_complex_LU_decomp(m, p, &s);

	gsl_linalg_complex_LU_solve(m, p, b, x);

	if (textout >= 3){
		printf("\nx = \n");
		gsl_vector_complex_fprintf(stdout, x, "%g");
	}

	for (int i = 0; i < 2 * N; i++){
	    alphas[i] = gsl_vector_complex_get(x, i);
    }

	gsl_permutation_free(p);
	gsl_vector_complex_free(x);

	return 0;
}


int solve_SLE_E(gsl_complex* alphas, gsl_complex* Qsj_zmax, params pars) {

	gsl_interp_accel *acc = gsl_interp_accel_alloc();

	int N = pars.N;

	for (int i = 0; i < 2 * N; i++)
		alphas[i] = gsl_complex_rect(0.0, 0.0);

	gsl_matrix_complex * m = gsl_matrix_complex_alloc(2 * N, 2 * N);
	gsl_vector_complex * b = gsl_vector_complex_alloc(2 * N);
	gsl_vector_complex * x = gsl_vector_complex_alloc(2 * N);

	for (int j = 0; j < 2 * N; j++) { 
		gsl_vector_complex_set(b, j, gsl_complex_rect(0.0, 0.0));
		for (int s = 0; s < 2 * N; s++)
			gsl_matrix_complex_set(m, j, s, gsl_complex_rect(0.0, 0.0));
	}

	gsl_complex RP, Mjs;
	for (int j = 0; j < N; j++) {

		double alpha_j = pars.k0*sin(pars.alpha) + j + pars.N1;
		double gamma_j = sqrt(pars.k0*pars.k0 - alpha_j*alpha_j);

		if (textout >= 3){
			cout << endl << "gamma_j" << endl << ">>> " << gamma_j << endl << endl;
		}


		RP = gsl_complex_mul(gsl_complex_mul(gsl_complex_rect(0.0, -2.0*gamma_j*sqrt(pars.a)), pars.C0), gsl_complex_exp(gsl_complex_rect(0.0, -gamma_j*pars.zmax)));
		if (textout >= 3){
			cout << endl << "RP" << endl << ">>> ";  print_gsl_complex(RP); cout << endl << endl;
			cout << endl << "RP1" << endl << ">>> ";  print_gsl_complex(pars.C0); cout << endl << endl;
		}
		//if (j + pars.N1 == 0)
		double CnjRE = gsl_spline_eval(pars.D2nj_re[j*N + (-pars.N1)], pars.zmax, acc);
        double CnjIM = gsl_spline_eval(pars.D2nj_im[j*N + (-pars.N1)], pars.zmax, acc);
		gsl_vector_complex_set(b, N + j, gsl_complex_mul(gsl_complex_rect(CnjRE, CnjIM),RP));


		//gsl_matrix_complex_set(m, j, j, gsl_complex_rect(0.0, gamma_j));
		gsl_matrix_complex_set(m, j, j + N, gsl_complex_rect(1.0, 0.0));
		for (int s = 0; s < N; s++) {

		// Here the new code needed!!!

          double alpha_s = pars.k0*sin(pars.alpha) + s + pars.N1;
		  double gamma_s = sqrt(pars.k0*pars.k0 - alpha_s*alpha_s);
		  double CnjRE = gsl_spline_eval(pars.D2nj_re[j*N + s], pars.zmin, acc);
          double CnjIM = gsl_spline_eval(pars.D2nj_im[j*N + s], pars.zmin, acc);
		  gsl_matrix_complex_set(m, j, s, gsl_complex_mul(gsl_complex_rect(CnjRE, CnjIM), gsl_complex_rect(0.0, gamma_s)) );
		}
		//gsl_matrix_complex_set(m, j, s, gsl_complex_mul(gsl_complex_rect(CnjRE, CnjIM), gsl_complex_rect(0.0, gamma_s)));

        // Here the new code needed!!!
		for (int s = 0; s < 2 * N; s++) {
          gsl_complex M3;
          M3 = gsl_complex_rect(0.0, 0.0);
          for( int k = 0; k < N; k++){
			double alpha_k = pars.k0*sin(pars.alpha) + k + pars.N1;
			double gamma_k = sqrt(pars.k0*pars.k0 - alpha_k*alpha_k);
			double CnjRE = gsl_spline_eval(pars.D2nj_re[j*N + k], pars.zmax, acc);
			double CnjIM = gsl_spline_eval(pars.D2nj_im[j*N + k], pars.zmax, acc);
			M3 = gsl_complex_add(M3, gsl_complex_mul(gsl_complex_rect(CnjRE, CnjIM), gsl_complex_mul(gsl_complex_rect(0.0, -gamma_k), Qsj_zmax[2 * N*s + k])));
          }
          Mjs = gsl_complex_add(Qsj_zmax[2 * N*(s) + j + N], M3);
          gsl_matrix_complex_set(m, j + N, s, Mjs);
		}

	}

	if (textout >= 3){
		printf("m = \n");
		gsl_matrix_complex_fprintf(stdout, m, "%g");

		printf("\nb = \n");
		gsl_vector_complex_fprintf(stdout, b, "%g");
	}

	int s;

	gsl_permutation * p = gsl_permutation_alloc(2 * N);

	gsl_linalg_complex_LU_decomp(m, p, &s);

	gsl_linalg_complex_LU_solve(m, p, b, x);

	if (textout >= 3){
		printf("\nx = \n");
		gsl_vector_complex_fprintf(stdout, x, "%g");
	}

	for (int i = 0; i < 2 * N; i++){
	    alphas[i] = gsl_vector_complex_get(x, i);
    }

	gsl_permutation_free(p);
	gsl_vector_complex_free(x);
	gsl_interp_accel_free(acc);

	return 0;
}


int solve_SLE(gsl_complex* alphas, gsl_complex* Qsj_zmax, params pars){

	if (pars.polar == 1){
		return solve_SLE_H( alphas, Qsj_zmax, pars);
	}else{
		return solve_SLE_E( alphas, Qsj_zmax, pars);
	}
	return 1;
}

int print_splineMatrix_toFile( gsl_spline** C1nj_re, gsl_spline** C1nj_im, double z1, double z2, int N, int n, int j, const char* mname){
  ofstream myfile;
  gsl_interp_accel *acc = gsl_interp_accel_alloc();

  string nullname = "", fname;
  stringstream nstr, jstr;
  nstr << n;
  nstr << j;

  // REAL
  fname = nullname + mname + "_" + nstr.str() + jstr.str() + "_re.txt";
  myfile.open(fname.c_str());
  for (double xi = z1; xi < z2; xi += 0.01)
  {
	  double yi_re = gsl_spline_eval(C1nj_re[N*n+j], xi, acc);
	  double yi_im = gsl_spline_eval(C1nj_im[N*n+j], xi, acc);
	  myfile << GSL_REAL(gsl_complex_rect(yi_re, yi_im)) << endl;
  }
  myfile.close();
  // IMAG
  fname = nullname + mname + "_" + nstr.str() + jstr.str() + "_im.txt";
  myfile.open(fname.c_str());
  for (double xi = z1; xi < z2; xi += 0.01)
  {
	  double yi_re = gsl_spline_eval(C1nj_re[N*n+j], xi, acc);
	  double yi_im = gsl_spline_eval(C1nj_im[N*n+j], xi, acc);
	  myfile << GSL_IMAG(gsl_complex_rect(yi_re, yi_im)) << endl;
  }
  myfile.close();

  gsl_interp_accel_free(acc);

  return 0;
}


int print_splineMatrix_toFile( gsl_spline** C1nj_re, gsl_spline** C1nj_im, double z1, double z2, int N, const char* mname){

  for( int i1 = 0; i1 < N; i1++ ){
    for( int i2 = 0; i2 < N; i2++ ){
      print_splineMatrix_toFile( C1nj_re, C1nj_im, z1, z2, N, i1, i2, mname);
    }
  }
  return 0;
}


int calculation(params* exp0, results* res) {
  using namespace std;

  if(textout >= 2){
    cout<<"println_gsl_complex((*exp0).epsilon0);"<<endl<<">>> ";println_gsl_complex((*exp0).epsilon0);cout<<endl;
    cout<<"println_gsl_complex((*exp0).epsilon1);"<<endl<<">>> ";println_gsl_complex((*exp0).epsilon1);cout<<endl;
    cout<<"println_gsl_complex(psi_n(-0.5, 0, exp0));"<<endl<<">>> ";println_gsl_complex(psi_n(-0.5, 0, exp0));cout<<endl;
	cout << "N1" << endl << ">>> " << (*exp0).N1 << endl << endl;
	cout << "N2" << endl << ">>> " << (*exp0).N2 << endl << endl;
	cout << "N" << endl << ">>> " << (*exp0).N << endl << endl;
	cout << "layer_type" << endl << ">>> " << (*exp0).layer_type << endl << endl;
	cout << "i1" << endl << ">>> " << (*exp0).fg.i1 << endl << endl;
	cout << "A1" << endl << ">>> " << (*exp0).fg.A1 << endl << endl;
	cout << "zmin" << endl << ">>> " << (*exp0).zmin << endl << endl;
	cout << "zmax" << endl << ">>> " << (*exp0).zmax << endl << endl;
	cout << "lambda_n" << endl << ">>> " << lambda_n(0, *exp0) << endl << endl;
	cout << "polarisation" << endl << ">>> " << (*exp0).polar << endl << endl;
  }

  int N = (*exp0).N;
  int Nz = (*exp0).Nz;

  gsl_spline **Cnj_re, **Cnj_im;
  gsl_spline **C1nj_re, **C1nj_im;
  gsl_spline **D2nj_re, **D2nj_im;
  gsl_spline **C2nj_re, **C2nj_im;

  if( (*exp0).polar == 1 ){
    calculate_Cnj(&Cnj_re, &Cnj_im, exp0);
    (*exp0).Cnj_re = Cnj_re;
    (*exp0).Cnj_im = Cnj_im;
  }else{
	calculate_D2nj(&D2nj_re, &D2nj_im, exp0);
	(*exp0).D2nj_re = D2nj_re;
	(*exp0).D2nj_im = D2nj_im;
	calculate_C1nj(&C1nj_re, &C1nj_im, exp0);
	(*exp0).C1nj_re = C1nj_re;
	(*exp0).C1nj_im = C1nj_im;
	calculate_C2nj(&C2nj_re, &C2nj_im, exp0);
	(*exp0).C2nj_re = C2nj_re;
	(*exp0).C2nj_im = C2nj_im;
  }

  ofstream myfile;
  gsl_interp_accel *acc = gsl_interp_accel_alloc();

  if( FILEOUT >= 2){
  ////////////////////
  // TEST FOR C1nj(z) in the case of E-Polarization
    if( (*exp0).polar == 0 ){
      print_splineMatrix_toFile( C1nj_re, C1nj_im, (*exp0).zmin, (*exp0).zmax, N, 0, 0, "C1");
      if( N >= 2 ) print_splineMatrix_toFile( C1nj_re, C1nj_im, (*exp0).zmin, (*exp0).zmax, N, 1, 1, "C1");
      if( N >= 3 ) print_splineMatrix_toFile( C1nj_re, C1nj_im, (*exp0).zmin, (*exp0).zmax, N, 2, 2, "C1");

      print_splineMatrix_toFile( C1nj_re, C1nj_im, (*exp0).zmin, (*exp0).zmax, N, "C1");
      print_splineMatrix_toFile( C2nj_re, C2nj_im, (*exp0).zmin, (*exp0).zmax, N, "C2");
      print_splineMatrix_toFile( D2nj_re, D2nj_im, (*exp0).zmin, (*exp0).zmax, N, "D2");
    }
  ///////////////////

  ////////////////////
  // TEST FOR Cnj(z) in the case of H-Polarization
    if( (*exp0).polar == 1 ){
      print_splineMatrix_toFile( Cnj_re, Cnj_im, (*exp0).zmin, (*exp0).zmax, N, "C");
    }
  ///////////////////
  }

  //cout << "I am alive. PRE ode" << endl;

  // solve system of ODEs => Q1
  gsl_complex Qsj_zmax[2*N*2 * N];
  //Qsj_zmax = (double*)malloc(sizeof(double)*(2 * N * 2 * N));
  //gsl_spline** Qsj;
  //Qsj = (gsl_spline**)malloc(sizeof(gsl_spline*)*(2*N*2*N));
  gsl_complex QsjArr[2*N*2*N*Nz];

  for(int s = 0; s < N * 2; s++) {
	  solve_SODE(exp0, s, QsjArr + s * 2 * N * Nz, Qsj_zmax+s*2*N);
  }

  if (textout >= 2) {
	  cout << "Qsj_zmax" << endl << ">>> ";
	  for (int s = 0 ; s < 2 * N; s++) {
		  cout << "s = " << s << ":" << endl;
		  for (int i = 0; i < 2 * N; i++)
			  println_gsl_complex(Qsj_zmax[i+s*2*N]);
			  cout << " ";
		  cout << endl;
	  }
	  //cout << endl;
  }

  //cout << "I am alive." << endl;

  // finding alpha_s coeffiecients
  gsl_complex alphas[2 * N];
  solve_SLE(alphas, Qsj_zmax, *exp0);

  if(textout >= 2){
	  cout << "alpha[s]" << endl << ">>> ";
	  for (int i = 0; i < 2 * N; i++){
			  print_gsl_complex(alphas[i]);
			  cout << " ";
	  }
	  cout << endl << endl;
  }

  // Constructing B
  gsl_complex B[N * Nz];
  for (int j = 0; j < N; j++) {
	  for (int i = 0; i < Nz; i++) {
		  B[j * Nz + i] = gsl_complex_rect(0.0,0.0);
		  for (int s = 0; s < 2 * N; s++) {
			  gsl_complex add_t1 = gsl_complex_mul(alphas[s], QsjArr[s * 2 * N*Nz + Nz*j + i]);
			  gsl_complex add_t2 = gsl_complex_add(B[j * Nz + i], add_t1);
			  B[j * Nz + i] = add_t2;
		  }
	  }
  }

  int N1 = (*exp0).N1;
  int N2 = (*exp0).N2;
  int Nx = (*exp0).Nx;

  if ((*exp0).calcnum == 0) {
	  ofstream myfile1;
	  // B0
	  myfile1.open("B0_abs.txt");
	  for (int i = 0; i < Nz; i++) {
		  double z = ((*exp0).zmax - (*exp0).zmin) / (Nz - 1)*i + (*exp0).zmin;
		  myfile1 << z << " " << gsl_complex_abs(B[(-N1)*Nz + i]) << endl;
	  }
	  myfile1.close();
	  // B0
	  myfile1.open("B0_arg.txt");
	  for (int i = 0; i < Nz; i++) {
		  double z = ((*exp0).zmax - (*exp0).zmin) / (Nz - 1)*i + (*exp0).zmin;
		  myfile1 << z << " " << gsl_complex_arg(B[(-N1) * Nz + i]) << endl;
	  }
	  myfile1.close();

  }

  // epsilon
  if (layerout){
	  print_layer(*exp0);
  }


  // Calculating V, D, def
  double D = 0.0, V = 0.0, def;

  for (int j = 0; j < N; j++) {
	  D += gsl_complex_abs(B[j*Nz + 0])*gsl_complex_abs(B[j*Nz + 0]);
	  if (j + (*exp0).N1 != 0)
		  V += gsl_complex_abs(B[j*Nz + Nz - 1])*gsl_complex_abs(B[j*Nz + Nz - 1]);
	  else {
		  gsl_complex u0 = gsl_complex_mul(gsl_complex_rect(sqrt((*exp0).a), 0.0), gsl_complex_mul((*exp0).C0, gsl_complex_exp(gsl_complex_rect(0.0, -cos((*exp0).alpha)*(*exp0).zmax))));
		  V += gsl_complex_abs(gsl_complex_sub(B[j*Nz + Nz - 1], u0))*gsl_complex_abs(gsl_complex_sub(B[j*Nz + Nz - 1], u0));
	  }
  }
  D /= gsl_complex_abs((*exp0).C0)*gsl_complex_abs((*exp0).C0)*(*exp0).a;
  V /= gsl_complex_abs((*exp0).C0)*gsl_complex_abs((*exp0).C0)*(*exp0).a;
  def = 1 - D - V;

  if (textout >= 2){
	  cout << "D" << endl << ">>> " << D << endl << endl;
	  cout << "V" << endl << ">>> " << V << endl << endl;
	  cout << "def" << endl << ">>> " << def << endl << endl;
  }

  // Constructing solution u(x,z)
  // on [0,a]x[z_min,z_max]
  //
  //int Nx = (*exp0).Nx;
  //gsl_complex u[Nx * Nz];
  gsl_complex * u = (*res).u;
  double x, z;
  type_paramsC0nj parC0nj;
  init_paramsC0nj(&parC0nj, *exp0);
  for (int ix = 0; ix < Nx; ix++) {
	  for (int iz = 0; iz < Nz; iz++) {
		  u[ix * Nz + iz] = gsl_complex_rect(0.0,0.0);
		  x = (*exp0).a / (Nx - 1)*ix;
		  z = (*exp0).zmin + ((*exp0).zmax - (*exp0).zmin) / (Nz - 1)*iz;
		  for (int j = 0; j < N; j++) {
			  gsl_complex add_t1 = gsl_complex_mul(B[j * Nz + iz], psi_n(x, j, &parC0nj));
			  gsl_complex add_t2 = gsl_complex_add(u[ix * Nz + iz], add_t1);
			  u[ix * Nz + iz] = add_t2;
		  }
	  }
  }

  (*res).D = D;
  (*res).V = V;
  (*res).def = def;

  //(*res).u = u;
  /*
  (*res).u = (gsl_complex*)malloc(sizeof(gsl_complex)*Nx * Nz);
  for (int i = 0; i < Nx*Nz; i++) {
	  ((*res).u)[i] = u[i];
  }
  */

  return 0;
}

int calc_16_07_17_ICNAAM16_NormalIncidenceCompareWithAnalyt(){

	// COMPARISON WITH ANALYTICAL SOLUTION
	params exp0;
	results res;

	exp0.lambda = 2 * M_PI; // wavelength
	exp0.alpha = 0.0*M_PI / 180.0; // angle of plane wave direction
	exp0.a = 2 * M_PI; // period
	exp0.C0 = gsl_complex_exp(gsl_complex_rect(0.0, 1.0)); // amplitude of the incedent wave
	exp0.epsilon0_re = 1.0; // inductive capacity for vacuum - dielectricheskaja pronicaemost'
	exp0.epsilon0_im = 0.0; // inductive capacity for vacuum - dielectricheskaja pronicaemost'
	exp0.mu0 = 1.0; // magnitic inductivity for vacuum - magnitnaja pronicaemost'
	exp0.polar = 1;

					 // \varepsilon(x,z)
	//exp0.epsN = 1; // type of periodic structure 1 - layer, 2 - sin
	exp0.epsilon1_re = 3.0; // inductive capacity for the periodic structure - real part
	exp0.epsilon1_im = 0.0; // inductive capacity for the periodic structure - complex part
							  // type 1 - plane homogeneous layer
							  // type 2 - [z1, z2 - A + A*sin(w*x)] homogeneous layer
	exp0.layer_type = 2;
	exp0.z1 = 0.0; // start of layer
	exp0.z2 = 1.0; // end of layer
	exp0.A1 = 0.0;
	exp0.w1 = 1.0;

	exp0.Nz = 100;
	exp0.Nx = 100;

	// DEPENDENT PARAMETERS
	dep_params_calc(&exp0);

	exp0.zmin = -1.0; // start of calculation domain
	exp0.zmax = 2.0; // end of calculation domain

	calculation(&exp0, &res);

	cout << "I am alive." << endl;

	gsl_complex* u = res.u;

	gsl_complex d1 = gsl_complex_rect(0.860335, 0.121198);
	gsl_complex d2 = gsl_complex_rect(-0.121198, 0.860335);
	gsl_complex d3 = gsl_complex_rect(0.496714, 0.069974);
	gsl_complex d4 = gsl_complex_rect(-0.121198, 0.860335);
	gsl_complex d5 = gsl_complex_rect(0.466239, -0.863314);
	gsl_complex d6 = gsl_complex_rect(0.217291, 1.2167);

	int Nx = exp0.Nx;
	int Nz = exp0.Nz;
	double err_max = 0.0;
	for (int ix = 0; ix < Nx; ix++) {
		for (int iz = 0; iz < Nz; iz++) {
			double x = (exp0).a / (Nx - 1)*ix;
			double z = (exp0).zmin + ((exp0).zmax - (exp0).zmin) / (Nz - 1)*iz;
			gsl_complex u_anal = gsl_complex_rect(0.0, 0.0);
			if (z < 0) {
				u_anal = gsl_complex_add(gsl_complex_mul(d1, gsl_complex_rect(sin(z), 0.0)), gsl_complex_mul(d2, gsl_complex_rect(cos(z), 0.0)));
			}
			else if (z >= 0 && z <= 1) {
				u_anal = gsl_complex_add(gsl_complex_mul(d3, gsl_complex_rect(sin(sqrt(3)*z), 0.0)), gsl_complex_mul(d4, gsl_complex_rect(cos(sqrt(3)*z), 0.0)));
			}
			else {
				u_anal = gsl_complex_add(gsl_complex_mul(d5, gsl_complex_rect(sin(z), 0.0)), gsl_complex_mul(d6, gsl_complex_rect(cos(z), 0.0)));
			}
			double err_cur = gsl_complex_abs(gsl_complex_sub(u[ix * Nz + iz], u_anal));
			if (err_max < err_cur) {
				err_max = err_cur;
			}
		}
	}
	if (textout >= 2) {
		cout << "MAX|u_comp - u_an| = " << err_max << endl << endl;
	}

	return 0;

}


int output_data(params exp0) {

	print_params(exp0);


	return 0;
}

int calc_16_07_18_ICNAAM16_DefectFromA1() {
	params exp0;
	results res;

	exp0.lambda = 2 * M_PI; // wavelength
	exp0.alpha = 30.0*M_PI / 180.0; // angle of plane wave direction
	exp0.a = 2 * M_PI; // period
	exp0.C0 = gsl_complex_exp(gsl_complex_rect(0.0, 1.0)); // amplitude of the incedent wave
	exp0.epsilon0_re = 1.0; // inductive capacity for vacuum - dielectricheskaja pronicaemost'
	exp0.epsilon0_im = 0.0; // inductive capacity for vacuum - dielectricheskaja pronicaemost'
	exp0.mu0 = 1.0; // magnitic inductivity for vacuum - magnitnaja pronicaemost'
	exp0.zmin = -1.0; // start of calculation domain
	exp0.zmax = 4.0; // end of calculation domain
	exp0.polar = 1; // H-polarization

					 // \varepsilon(x,z)
	//exp0.epsN = 1; // type of periodic structure 1 - layer, 2 - sin
	exp0.epsilon1_re = 42.0; // inductive capacity for the periodic structure - real part
	exp0.epsilon1_im = 115.0; // inductive capacity for the periodic structure - complex part
							// type 1 - plane homogeneous layer
							// type 2 - [z1, z2 - A + A*sin(w*x)] homogeneous layer
	exp0.layer_type = 2;
	exp0.z1 = 0.0; // start of layer
	exp0.z2 = 3.0; // end of layer
	//exp0.A1 = 0.1;
	//exp0.w1 = 1.0;
	
	exp0.layer_type = 2;
	exp0.fg.i1 = 1.0;
	exp0.fg.i1 = 1.0;

	exp0.Nz = 300;
	exp0.Nx = 300;

	// DEPENDENT PARAMETERS
	dep_params_calc(&exp0);

	int Nx = exp0.Nx;
	int Nz = exp0.Nz;
	res.u = (gsl_complex*)malloc(sizeof(gsl_complex)*Nx*Nz);

	//load_data(&exp0);
	int NA = 11;
	double defs[NA];
	for (int i = 0; i < NA; i++) {
		exp0.A1 = 1.0 / (NA - 1)*i;
		calculation(&exp0, &res);
		cout << "A1 = " << exp0.A1 << "     def = " << res.def << endl;
		defs[i] = res.def;
		//free(res.u);
	}

	ofstream myfile1;
	myfile1.open("figDefFromA1.txt");
	for (int i = 0.0; i < NA; i++) {
		myfile1 << defs[i] << endl;
	}
	myfile1.close();

	//exp.A1 = 0.5;
	//calculation(&exp, &res);

	if (textout >= 1) {
		output_data(exp0);
	}

}


int calc_17_06_20_DD17_DefectFromA1() {
	params exp0;
	results res;

	exp0.lambda = 2 * M_PI; // wavelength
	exp0.alpha = 30.0*M_PI / 180.0; // angle of plane wave direction
	exp0.a = 2 * M_PI; // period
	exp0.C0 = gsl_complex_exp(gsl_complex_rect(0.0, 1.0)); // amplitude of the incedent wave
	exp0.epsilon0_re = 1.0; // inductive capacity for vacuum - dielectricheskaja pronicaemost'
	exp0.epsilon0_im = 0.0; // inductive capacity for vacuum - dielectricheskaja pronicaemost'
	exp0.mu0 = 1.0; // magnitic inductivity for vacuum - magnitnaja pronicaemost'
	exp0.zmin = -1.0; // start of calculation domain
	exp0.zmax = 4.0; // end of calculation domain
	exp0.polar = 1; // H-polarization

									// \varepsilon(x,z)
									//exp0.epsN = 1; // type of periodic structure 1 - layer, 2 - sin
	exp0.epsilon1_re = 42.0; // inductive capacity for the periodic structure - real part
	exp0.epsilon1_im = 115.0; // inductive capacity for the periodic structure - complex part
														// type 1 - plane homogeneous layer
														// type 2 - [z1, z2 - A + A*sin(w*x)] homogeneous layer
	exp0.layer_type = 3;
	exp0.z1 = 0.0; // start of layer
	exp0.z2 = 3.0; // end of layer
	exp0.A1 = 0.1;
	exp0.w1 = 1.0;

	exp0.layer_type = 3;

	exp0.fg.B0 = 3.0;
	exp0.fg.D0 = 0.0;

	exp0.fg.i1 = 1.0;
	exp0.fg.i2 = 0.0;
	exp0.fg.i3 = 0.0;
	exp0.fg.j1 = 0.0;
	exp0.fg.j2 = 0.0;
	exp0.fg.j3 = 0.0;

	exp0.fg.A1 = 0.1;
	exp0.fg.A2 = 0.0;
	exp0.fg.A3 = 0.0;
	exp0.fg.B1 = 0.0;
	exp0.fg.B2 = 0.0;
	exp0.fg.B3 = 0.0;

	exp0.fg.C1 = 0.0;
	exp0.fg.C2 = 0.0;
	exp0.fg.C3 = 0.0;
	exp0.fg.D1 = 0.0;
	exp0.fg.D2 = 0.0;
	exp0.fg.D3 = 0.0;

	//double i1, i2, i3;
	//double A1, A2, A3, B1, B2, B3, B0;
	//double j1, j2, j3;
	//double C1, C2, C3, D1, D2, D3, D0;


	exp0.Nz = 300;
	exp0.Nx = 300;

	// DEPENDENT PARAMETERS
	dep_params_calc(&exp0);

	int Nx = exp0.Nx;
	int Nz = exp0.Nz;
	res.u = (gsl_complex*)malloc(sizeof(gsl_complex)*Nx*Nz);

	//load_data(&exp0);
	int NA = 2;
	double A1max = 0.1; 
	double defs[NA];
	for (int i = 0; i < NA; i++) {
		exp0.calcnum = i;
		exp0.fg.A1 = A1max / (NA - 1)*i;
		calculation(&exp0, &res);
		cout << "A1 = " << exp0.fg.A1 << "     def = " << res.def << endl;
		defs[i] = res.def;
		//free(res.u);
	}

	ofstream myfile1;
	myfile1.open("figDefFromA1.txt");
	for (int i = 0.0; i < NA; i++) {
		myfile1 << defs[i] << endl;
	}
	myfile1.close();

	//exp.A1 = 0.5;
	//calculation(&exp, &res);

	if (textout >= 1) {
		output_data(exp0);
	}

}



int add_parameter(params* exp0, string key, string value) {
	if (key == string("A1")) {
		(*exp0).fg.A1 = atof(value.c_str());
	}
	else if (key == string("A2")) {
		(*exp0).fg.A2 = atof(value.c_str());
	}
	else if (key == string("A3")) {
		(*exp0).fg.A3 = atof(value.c_str());
	}
	else if (key == string("B1")) {
		(*exp0).fg.B1 = atof(value.c_str());
	}
	else if (key == string("B2")) {
		(*exp0).fg.B2 = atof(value.c_str());
	}
	else if (key == string("B3")) {
		(*exp0).fg.B3 = atof(value.c_str());
	}
	else if (key == string("C1")) {
		(*exp0).fg.C1 = atof(value.c_str());
	}
	else if (key == string("C2")) {
		(*exp0).fg.C2 = atof(value.c_str());
	}
	else if (key == string("C3")) {
		(*exp0).fg.C3 = atof(value.c_str());
	}
	else if (key == string("D1")) {
		(*exp0).fg.D1 = atof(value.c_str());
	}
	else if (key == string("D2")) {
		(*exp0).fg.D2 = atof(value.c_str());
	}
	else if (key == string("D3")) {
		(*exp0).fg.D3 = atof(value.c_str());
	}
	else if (key == string("B0")) {
		(*exp0).fg.B0 = atof(value.c_str());
	}
	else if (key == string("D0")) {
		(*exp0).fg.D0 = atof(value.c_str());
	}
	else if (key == string("i1")) {
		(*exp0).fg.i1 = atof(value.c_str());
		if (textout >= 3) cout << "input i1: " << (*exp0).fg.i1 << endl;
	}
	else if (key == string("i2")) {
		(*exp0).fg.i2 = atof(value.c_str());
	}
	else if (key == string("i3")) {
		(*exp0).fg.i3 = atof(value.c_str());
	}
	else if (key == string("j1")) {
		(*exp0).fg.j1 = atof(value.c_str());
	}
	else if (key == string("j2")) {
		(*exp0).fg.j2 = atof(value.c_str());
	}
	else if (key == string("j3")) {
		(*exp0).fg.j3 = atof(value.c_str());
	}

	else if (key == string("alpha")) {
		(*exp0).alpha = atof(value.c_str())*M_PI / 180.0;;
	}
	else if (key == string("lam")) {
		(*exp0).lambda = atof(value.c_str());
	}
	else if (key == string("eps_re")) {
		(*exp0).epsilon1_re = atof(value.c_str());
	}
	else if (key == string("eps_im")) {
		(*exp0).epsilon1_im = atof(value.c_str());
	}
	else if (key == string("pol")) {
		if (value == string("H"))
			(*exp0).polar = 1;
		else
			(*exp0).polar = 0;
		if (textout >= 3) cout << endl << "POLARIZATION LOADED = " << (*exp0).polar << endl << endl;
	}
	else if (key == string("M")) {
		(*exp0).Nx = atof(value.c_str());
	}
	else if (key == string("N")) {
		(*exp0).Nz = atof(value.c_str());
	}
	else if (key == string("b")) {
		(*exp0).a = atof(value.c_str());
	}

	return 0;
}

int load_parameters(string instr, params* exp0) {

	// BASIC PARAMETERS
	load_data(exp0);

	(*exp0).C0 = gsl_complex_exp(gsl_complex_rect(1.0, 0.0)); // amplitude of the incedent wave
	(*exp0).mu0 = 1.0; // magnitic inductivity for vacuum - magnitnaja pronicaemost'
	//(*exp0).layer_type = 3;

	// LOADED PARAMETERS
	//ifstream infile;
	string key, value;
	char c;
	int k = 0;
	bool foundel = false;
	//infile.open("input.txt");
	std::stringstream infile;
	infile.str(instr);
	if (infile >> c) {
		while (infile >> key && infile >> value)
		{
			value = value.substr(0, value.find(','));
			if (string::npos != value.find('}')) {
				foundel = true;
			}
			value = value.substr(0, value.find('}'));
			key = key.substr(1, key.length() - 3);
			if( textout >=3 ) cout << key << "   " << value << endl;
			add_parameter(exp0, key, value);
			if (foundel) {
				k++;
				foundel = false;
			}
		}
	}
	//infile.close();


	// DEPENDENT PARAMETERS
	dep_params_calc(exp0);

	return 0;
}

int load_experiments(params** exps, int* NE_) {
	int NE = 0;
	std::ifstream file("input.txt");
	std::string str;
	while (std::getline(file, str))
	{
		NE++;
	}
	if (textout >= 2) cout << "NE = " << NE << endl;
	(*exps) = (params*)malloc(sizeof(params)*NE);

	std::ifstream file1("input.txt");
	int i = 0;
	while (std::getline(file1, str))
	{
		load_parameters(str, (*exps) + i);
		(*exps)[i].calcnum = i;
		i++;
	}

	*NE_ = NE;

	return 0;
}

int exps_computations_SP( params* exps, int NE, results* ress) {
	for (int i = 0; i < NE; i++) {
		int Nx = exps[i].Nx;
		int Nz = exps[i].Nz;
		ress[i].u = (gsl_complex*)malloc(sizeof(gsl_complex)*Nx*Nz);

		calculation(exps + i, ress + i);
		cout << "calc num = " << i << ":  absorbed,d = " << ress[i].def << ";  passed,D = " << ress[i].D << ";  reflected,V = " << ress[i].V << endl;
		//defs[i] = res.def;
		//free(res.u);
	}

	return 0;
}


#if GO_MPI == 1

int exps_computations_MPI(params* exps, int NE, results* ress) {

	//////////////////////////////////
	/// DEVELOP MPI EXECUTION HERE ///
	//////////////////////////////////

  int myrank, np;
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  MPI_Comm_size(MPI_COMM_WORLD, &np);

  int my_start_i = (int)(NE*1.0/np*myrank);
  int my_end_i = ((int)(NE*1.0/np*(myrank+1)))-1;

  cout << "my rank is " << myrank << ": mystart_i = " << my_start_i << ", my_end_i = " << my_end_i << endl;

  int displs[np];
  int count[np];
  for(int i = 0; i < np; i++){
    displs[i] = (int)(NE*1.0/np*i);
    count[i] = ((int)(NE*1.0/np*(i+1)))-1 - (int)(NE*1.0/np*i) + 1;
  }

  double defs[NE], Vs[NE], Ds[NE];

  for (int i = my_start_i; i <= my_end_i; i++) {
    int Nx = exps[i].Nx;
    int Nz = exps[i].Nz;
    ress[i].u = (gsl_complex*)malloc(sizeof(gsl_complex)*Nx*Nz);

    calculation(exps + i, ress + i);
    cout << "calc num = " << i << ":  def = " << ress[i].def << ", ref = " << ress[i].V << ", trans = " << ress[i].D << endl;
    //defs[i] = res.def;
    //free(res.u);
    defs[i] = ress[i].def;
    Vs[i] = ress[i].V;
    Ds[i] = ress[i].D;
  }

  MPI_Barrier( MPI_COMM_WORLD );
  MPI_Gatherv ( defs + displs[myrank], count[myrank],  MPI_DOUBLE, defs, count, displs, MPI_DOUBLE, 0, MPI_COMM_WORLD );
  MPI_Gatherv ( Vs + displs[myrank], count[myrank],  MPI_DOUBLE, Vs, count, displs, MPI_DOUBLE, 0, MPI_COMM_WORLD );
  MPI_Gatherv ( Ds + displs[myrank], count[myrank],  MPI_DOUBLE, Ds, count, displs, MPI_DOUBLE, 0, MPI_COMM_WORLD );
  MPI_Barrier( MPI_COMM_WORLD );

  if(myrank == 0){
    for(int i = 0; i < NE; i++) ress[i].def = defs[i];
    for(int i = 0; i < NE; i++) ress[i].V = Vs[i];
    for(int i = 0; i < NE; i++) ress[i].D = Ds[i];
  }

  return 0;


	return 0;
}

#endif





int exps_computations(params* exps, int NE, results* ress) {
#if GO_MPI == 1
	return exps_computations_MPI(exps, NE, ress);
#else
	return exps_computations_SP(exps, NE, ress);
#endif
}

int calc_17_05_09_KOSMONIT_def_from_alpha() {
	params* exps;
	results* ress;
	int NE;

	load_experiments(&exps, &NE);

	ress = (results*)malloc(sizeof(results)*NE);

	exps_computations(exps, NE, ress);


	int myrank, np;
#if GO_MPI == 1
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
	MPI_Comm_size(MPI_COMM_WORLD, &np);
	//	cout << "my rank is" << myrank << endl;
#else
	myrank = 0;
	np = 1;
#endif

	if (myrank == 0) {
		ofstream myfile1;
		myfile1.open("output.txt");
		for (int i = 0; i < NE; i++) {
			myfile1 << "{'defect': " << ress[i].def << ", 'passed': " << ress[i].D << ", 'reflected': " << ress[i].V << "}" << endl;
		}
		myfile1.close();
		//ress[0].u
	}

	int NA = 11;
	double defs[NA];
	double alphas[NA];
	double alpha0 = exps[0].alpha;
	for (int i = 0; i < NA; i++) {
		exps[0].alpha = alpha0 / (NA - 1)*i;
		//calculation(&exp0, &res);
		exps_computations(exps, 1, ress);
		cout << "A1 = " << exps[0].A1 << "     def = " << ress[0].def << endl;
		defs[i] = ress[0].def;
		alphas[i] = exps[0].alpha;
		//free(res.u);
	}

	ofstream myfile2;
	myfile2.open("KOSMONIT_figDefFromAlpha_alpha.txt");
	for (int i = 0.0; i < NA; i++) {
		myfile2 << alphas[i] << endl;
	}
	myfile2.close();

	ofstream myfile3;
	myfile3.open("KOSMONIT_figDefFromAlpha_def.txt");
	for (int i = 0.0; i < NA; i++) {
		myfile3 << defs[i] << endl;
	}
	myfile3.close();


	if (textout >= 1) {
		//		output_data(exps[1]);
	}

	/*
	load_data(&exp0);

	int NA = 11;
	double defs[NA];
	for (int i = 0; i < NA; i++) {
		exp0.A1 = 1.0 / (NA - 1)*i;
		calculation(&exp0, &res);
		cout << "A1 = " << exp0.A1 << "     def = " << res.def << endl;
		defs[i] = res.def;
		//free(res.u);
	}

	ofstream myfile1;
	myfile1.open("figDefFromA1.txt");
	for (int i = 0.0; i < NA; i++) {
		myfile1 << defs[i] << endl;
	}
	myfile1.close();
	*/
	return 0;
}

int calc_16_07_31_SSSE16_processing_of_input_txt() {
	params* exps;
	results* ress;
	int NE;

	load_experiments( &exps, &NE );

	ress = (results*)malloc(sizeof(results)*NE);

	exps_computations( exps, NE, ress);


  int myrank, np;
#if GO_MPI == 1
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
	MPI_Comm_size(MPI_COMM_WORLD, &np);
//	cout << "my rank is" << myrank << endl;
#else
  myrank = 0;
  np = 1;
#endif

  if(myrank == 0){
	ofstream myfile1;
	myfile1.open("output.txt");
	for (int i = 0; i < NE; i++) {
		myfile1 << "{'defect': " << ress[i].def << ", 'passed': " << ress[i].D << ", 'reflected': " << ress[i].V << "}" << endl;
	}
	myfile1.close();

	//ress[0].u
}
	if (textout >= 1) {
//		output_data(exps[1]);
	}

}


int calc_17_06_19_DD17_NormalIncidenceCompareWithAnalyt(){
	params* exps;
	results* ress;
	int NE;

	load_experiments( &exps, &NE );

	exps[0].lambda = 2 * M_PI; // wavelength
	exps[0].alpha = 0.0*M_PI / 180.0; // angle of plane wave direction
	exps[0].a = 2 * M_PI; // period
	exps[0].C0 = gsl_complex_exp(gsl_complex_rect(0.0, 1.0)); // amplitude of the incedent wave
	exps[0].epsilon0_re = 1.0; // inductive capacity for vacuum - dielectricheskaja pronicaemost'
	exps[0].epsilon0_im = 0.0; // inductive capacity for vacuum - dielectricheskaja pronicaemost'
	exps[0].mu0 = 1.0; // magnitic inductivity for vacuum - magnitnaja pronicaemost'
	exps[0].polar = 0;

					 // \varepsilon(x,z)
	//exp0.epsN = 1; // type of periodic structure 1 - layer, 2 - sin
	exps[0].epsilon1_re = 3.0; // inductive capacity for the periodic structure - real part
	exps[0].epsilon1_im = 0.0; // inductive capacity for the periodic structure - complex part
							  // type 1 - plane homogeneous layer
							  // type 2 - [z1, z2 - A + A*sin(w*x)] homogeneous layer

	// DEPENDENT PARAMETERS
	dep_params_calc(exps);

	exps[0].zmin = -1.0; // start of calculation domain
	exps[0].zmax = 2.0; // end of calculation domain
	exps[0].z1 = -1.0; // start of calculation domain
	exps[0].z2 = 2.0; // end of calculation domain
	exps[0].C0 = gsl_complex_exp(gsl_complex_rect(0.0, 1.0));


	exps[1].lambda = 2 * M_PI; // wavelength
	exps[1].alpha = 0.0*M_PI / 180.0; // angle of plane wave direction
	exps[1].a = 2 * M_PI; // period
	exps[1].C0 = gsl_complex_exp(gsl_complex_rect(0.0, 1.0)); // amplitude of the incedent wave
	exps[1].epsilon0_re = 1.0; // inductive capacity for vacuum - dielectricheskaja pronicaemost'
	exps[1].epsilon0_im = 0.0; // inductive capacity for vacuum - dielectricheskaja pronicaemost'
	exps[1].mu0 = 1.0; // magnitic inductivity for vacuum - magnitnaja pronicaemost'
	exps[1].polar = 1;

					 // \varepsilon(x,z)
	//exp0.epsN = 1; // type of periodic structure 1 - layer, 2 - sin
	exps[1].epsilon1_re = 3.0; // inductive capacity for the periodic structure - real part
	exps[1].epsilon1_im = 0.0; // inductive capacity for the periodic structure - complex part
							  // type 1 - plane homogeneous layer
							  // type 2 - [z1, z2 - A + A*sin(w*x)] homogeneous layer

	dep_params_calc(exps+1);

	exps[1].zmin = -1.0;
	exps[1].zmax = 2.0;
	exps[1].z1 = -1.0; // start of calculation domain
	exps[1].z2 = 2.0; // end of calculation domain
	exps[1].C0 = gsl_complex_exp(gsl_complex_rect(0.0, 1.0));

	ress = (results*)malloc(sizeof(results)*NE);

	exps_computations( exps, NE, ress);


  int myrank, np;
#if GO_MPI == 1
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
	MPI_Comm_size(MPI_COMM_WORLD, &np);
//	cout << "my rank is" << myrank << endl;
#else
  myrank = 0;
  np = 1;
#endif

  if(myrank == 0){
	ofstream myfile1;
	myfile1.open("output.txt");
	for (int i = 0; i < NE; i++) {
		myfile1 << "{'defect': " << ress[i].def << ", 'passed': " << ress[i].D << ", 'reflected': " << ress[i].V << "}" << endl;
	}
	myfile1.close();

	//ress[0].u
}
	if (textout >= 1) {
//		output_data(exps[1]);
	}


	gsl_complex d1 = gsl_complex_rect(0.860335, 0.121198);
	gsl_complex d2 = gsl_complex_rect(-0.121198, 0.860335);
	gsl_complex d3 = gsl_complex_rect(0.496714, 0.069974);
	gsl_complex d4 = gsl_complex_rect(-0.121198, 0.860335);
	gsl_complex d5 = gsl_complex_rect(0.466239, -0.863314);
	gsl_complex d6 = gsl_complex_rect(0.217291, 1.2167);



	params exp0 = exps[0];
	gsl_complex* u = ress[0].u;

	int Nx = exp0.Nx;
	int Nz = exp0.Nz;
	double err_max = 0.0;
	for (int ix = 0; ix < Nx; ix++) {
		for (int iz = 0; iz < Nz; iz++) {
			double x = (exp0).a / (Nx - 1)*ix;
			double z = (exp0).zmin + ((exp0).zmax - (exp0).zmin) / (Nz - 1)*iz;
			gsl_complex u_anal = gsl_complex_rect(0.0, 0.0);
			if (z < 0) {
				u_anal = gsl_complex_add(gsl_complex_mul(d1, gsl_complex_rect(sin(z), 0.0)), gsl_complex_mul(d2, gsl_complex_rect(cos(z), 0.0)));
			}
			else if (z >= 0 && z <= 1) {
				u_anal = gsl_complex_add(gsl_complex_mul(d3, gsl_complex_rect(sin(sqrt(3)*z), 0.0)), gsl_complex_mul(d4, gsl_complex_rect(cos(sqrt(3)*z), 0.0)));
			}
			else {
				u_anal = gsl_complex_add(gsl_complex_mul(d5, gsl_complex_rect(sin(z), 0.0)), gsl_complex_mul(d6, gsl_complex_rect(cos(z), 0.0)));
			}
			double err_cur = gsl_complex_abs(gsl_complex_sub(u[ix * Nz + iz], u_anal));
			if (err_max < err_cur) {
				err_max = err_cur;
			}
		}
	}
	if (textout >= 1) {
		cout << "H-pol:   MAX|u_comp - u_an| = " << err_max << endl << endl;
	}


	exp0 = exps[1];
	u = ress[1].u;

	Nx = exp0.Nx;
	Nz = exp0.Nz;
	err_max = 0.0;
	for (int ix = 0; ix < Nx; ix++) {
		for (int iz = 0; iz < Nz; iz++) {
			double x = (exp0).a / (Nx - 1)*ix;
			double z = (exp0).zmin + ((exp0).zmax - (exp0).zmin) / (Nz - 1)*iz;
			gsl_complex u_anal = gsl_complex_rect(0.0, 0.0);
			if (z < 0) {
				u_anal = gsl_complex_add(gsl_complex_mul(d1, gsl_complex_rect(sin(z), 0.0)), gsl_complex_mul(d2, gsl_complex_rect(cos(z), 0.0)));
			}
			else if (z >= 0 && z <= 1) {
				u_anal = gsl_complex_add(gsl_complex_mul(d3, gsl_complex_rect(sin(sqrt(3)*z), 0.0)), gsl_complex_mul(d4, gsl_complex_rect(cos(sqrt(3)*z), 0.0)));
			}
			else {
				u_anal = gsl_complex_add(gsl_complex_mul(d5, gsl_complex_rect(sin(z), 0.0)), gsl_complex_mul(d6, gsl_complex_rect(cos(z), 0.0)));
			}
			double err_cur = gsl_complex_abs(gsl_complex_sub(u[ix * Nz + iz], u_anal));
			if (err_max < err_cur) {
				err_max = err_cur;
			}
		}
	}
	if (textout >= 1) {
		cout << "E-pol:   MAX|u_comp - u_an| = " << err_max << endl << endl;
	}


}

int print_singleParam(std::ofstream& myfile1, char* parname, double parval){
	myfile1 << ", '" << parname << "': " << parval;
}

int print_singleExp(std::ofstream& myfile1, params exp0){
	myfile1 << "{";
	myfile1 << "'D0': " << exp0.fg.D0;
	print_singleParam(myfile1, "B0", exp0.fg.B0);
	print_singleParam(myfile1, "A1", exp0.fg.A1);
	print_singleParam(myfile1, "A2", exp0.fg.A2);
	print_singleParam(myfile1, "A3", exp0.fg.A3);
	print_singleParam(myfile1, "C1", exp0.fg.C1);
	print_singleParam(myfile1, "C2", exp0.fg.C2);
	print_singleParam(myfile1, "C3", exp0.fg.C3);
	print_singleParam(myfile1, "B1", exp0.fg.B1);
	print_singleParam(myfile1, "B2", exp0.fg.B2);
	print_singleParam(myfile1, "B3", exp0.fg.B3);
	print_singleParam(myfile1, "D1", exp0.fg.D1);
	print_singleParam(myfile1, "D2", exp0.fg.D2);
	print_singleParam(myfile1, "D3", exp0.fg.D3);
	print_singleParam(myfile1, "i1", exp0.fg.i1);
	print_singleParam(myfile1, "i2", exp0.fg.i2);
	print_singleParam(myfile1, "i3", exp0.fg.i3);
	print_singleParam(myfile1, "j1", exp0.fg.j1);
	print_singleParam(myfile1, "j2", exp0.fg.j2);
	print_singleParam(myfile1, "j3", exp0.fg.j3);
	if (exp0.polar == 1){
		myfile1 << ", 'pol': H";
	}else{
		myfile1 << ", 'pol': E";
	}
	print_singleParam(myfile1, "b", exp0.a);
	print_singleParam(myfile1, "w", exp0.w);
	print_singleParam(myfile1, "N", exp0.Nx);
	print_singleParam(myfile1, "M", exp0.Nz);
	print_singleParam(myfile1, "alpha", exp0.alpha);
	print_singleParam(myfile1, "eps_re", exp0.epsilon1_re);
	print_singleParam(myfile1, "eps_im", exp0.epsilon1_im);
	print_singleParam(myfile1, "lam", exp0.lambda);
	myfile1 << "}" << endl;

	//'D0': 0.0, 'B0' : 6.2831853071795862, 
	// 'A1' : 0.1, 'A3' : 0.0, 'A2' : 0.0, 
	// 'C3' : 0.0, 'C2' : 0.0, 'C1' : 0.0, 
	// 'D2' : 0.0, 'D3' : 0.0, 'D1' : 0.0
	// 'B2' : 0.0
	// 'j1' : 0.0, 'j2' : 0.0 'j3' : 0.0,
	// 'i1' : 3.0, 'i3' : 0.0, 'i2' : 0.0, 
	//'b' : 6.2831853071795862, 'pol' : E,  'w' : 3.0,  'N' : 600, 'M' : 600, 'alpha' : 0.0, 
	//'eps_re' : 5.0, 'eps_im' : 2.0, 'lam' : 6.28318530717, 
	


}

int construct_experiment(params** exps, int* NE){
	int myrank, np;
#if GO_MPI == 1
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
	MPI_Comm_size(MPI_COMM_WORLD, &np);
	//	cout << "my rank is" << myrank << endl;
#else
	myrank = 0;
	np = 1;
#endif

	params expDef;
	std::string str;
	std::ifstream file1("input_def.txt");
	int i = 0;
	while(std::getline(file1, str))
	{
		load_parameters(str, &expDef);
		expDef.calcnum = i;
		i++;
	}

	*NE = 30;

	(*exps) = (params*)malloc(sizeof(params)*(*NE));

	(*exps)[0] = expDef;

	if (myrank == 0){
		std::ofstream myfile1;
		myfile1.open("input.txt");

		for (int i = 0; i < *NE; i++) {
			(*exps)[i] = expDef;
			(*exps)[i].alpha = i*2.3;
			print_singleExp( myfile1, (*exps)[i] );
		}

		myfile1.close();

		//ress[0].u
	}
}

int calc_17_06_19_DD17_UndergroundCylinder() {
	params* exps;
	results* ress;
	int NE;

	//load_experiments(&exps, &NE);
	construct_experiment(&exps, &NE);

	load_experiments(&exps, &NE);

	ress = (results*)malloc(sizeof(results)*NE);

	exps[0].Nx = 300;
	exps[0].Nz = 300;

 for (int i = 0; i < NE; i++) {
		exps[i].layer_type = 4;
		exps[i].fg.cylX = M_PI;
		exps[i].fg.cylZ = 4.5;
		exps[i].fg.cylR = 1.0;
	}

	exps_computations(exps, NE, ress);

		ofstream myfile1;
		myfile1.open("output.txt");
		for (int i = 0; i < NE; i++) {
			myfile1 << "{'defect': " << ress[i].def << ", 'passed': " << ress[i].D << ", 'reflected': " << ress[i].V << "}" << endl;
		}
		myfile1.close();

		ofstream myfile2;
		myfile2.open("output_defFromAlpha_nocyl.txt");
		for (int i = 0; i < NE; i++) {
			myfile2 << exps[i].alpha*180.0 / M_PI << " " << ress[i].def << endl;
		}
		myfile2.close();


		for (int i = 0; i < NE; i++) {
			exps[i].layer_type = 4;
			exps[i].fg.cylX = M_PI;
			exps[i].fg.cylZ = 4.5;
			exps[i].fg.cylR = 1.5;
		}

		exps_computations(exps, NE, ress);

		ofstream myfile3;
		myfile1.open("output.txt");
		for (int i = 0; i < NE; i++) {
			myfile3 << "{'defect': " << ress[i].def << ", 'passed': " << ress[i].D << ", 'reflected': " << ress[i].V << "}" << endl;
		}
		myfile3.close();

		ofstream myfile4;
		myfile4.open("output_defFromAlpha_cyl.txt");
		for (int i = 0; i < NE; i++) {
			myfile4 << exps[i].alpha*180.0/M_PI << " " << ress[i].def << endl;
		}
		myfile4.close();

}


int calc_current() {
	params* exps;
	results* ress;
	int NE;

	load_experiments( &exps, &NE );

	ress = (results*)malloc(sizeof(results)*NE);

	exps_computations( exps, NE, ress);


  int myrank, np;
#if GO_MPI == 1
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
	MPI_Comm_size(MPI_COMM_WORLD, &np);
//	cout << "my rank is" << myrank << endl;
#else
  myrank = 0;
  np = 1;
#endif

  if(myrank == 0){
	ofstream myfile1;
	myfile1.open("output.txt");
	for (int i = 0; i < NE; i++) {
		myfile1 << "{'defect': " << ress[i].def << ", 'passed': " << ress[i].D << ", 'reflected': " << ress[i].V << "}" << endl;
	}
	myfile1.close();

	//ress[0].u
}
	if (textout >= 1) {
//		output_data(exps[1]);
	}

}


int print_layer( params exp0){

	int Nz = 800;
	int Nx = 800;

	if (textout >= 2) {
		cout << "Experiment number = " << (exp0).calcnum << endl;
		cout << "Saving experiment sketch to file..." << endl;
	}

	ofstream myfile1;
	stringstream ss;
	ss << "exp" << (exp0).calcnum << "_epsilon.txt";
	string str = ss.str();
	myfile1.open(str.c_str());
	type_paramsC0nj parC0;
	type_paramsEps parsEps;
	init_paramsC0nj(&parC0, (exp0));
	init_paramsEps(&parsEps, parC0);
	//cout << endl << "period " << (*exp0).a << endl;
	//cout << endl << "zmax " << (*exp0).zmax << endl;
	for (int i = 0; i < Nz; i++) {
		double z = ((exp0).zmax - (exp0).zmin) / (Nz - 1)*i + (exp0).zmin;
		for (int j = 0; j < Nx; j++) {
			double x = ((exp0).a - 0.0) / (Nx - 1)*j + 0.0;
			myfile1 << z << " " << x << " " << gsl_complex_abs(epsilon(x, z, &parsEps)) << endl;
		}
		myfile1 << endl;
	}
	myfile1.close();


	return 0;
}


int dpproj_run(int argc, char **argv){

	using namespace std;

	int myrank = 0, np = 1;

// TEXT OUT

  textout = 1;

#if TEXTOUT == 0
  textout = 0;
#endif

#if TEXTOUT == 1
  textout = 1;
#endif

#if TEXTOUT == 2
  textout = 2;
#endif


textout = TEXTOUT;

// MPI

#if GO_MPI == 1
	MPI_Init(&argc, &argv);
#endif

#if GO_MPI == 1
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
	MPI_Comm_size(MPI_COMM_WORLD, &np);
//	cout << "my rank is" << myrank << endl;
#endif

	if (textout >= 1)
	if (myrank == 0)
	  cout << "number of processes = " << np << endl;

	double aclock1, aclock2;

	aclock1 = mclock();
	//calc_16_07_31_SSSE16_processing_of_input_txt();
	//calc_16_07_18_ICNAAM16_DefectFromA1();
	//calc_16_07_17_ICNAAM16_NormalIncidenceCompareWithAnalyt();
	//calc_17_05_09_KOSMONIT_def_from_alpha();
	//calc_17_06_20_DD17_DefectFromA1();
	//calc_17_06_19_DD17_NormalIncidenceCompareWithAnalyt();
	//calc_17_06_19_DD17_UndergroundCylinder();
	calc_current();
	aclock2 = mclock();

	if (textout >= 1)
	if (myrank == 0) cout << "Total time: " << diffclock(aclock1, aclock2) << "s" << endl << endl;


#if GO_MPI == 1
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Finalize();
#endif

  return 0;

}
